import json
import math
import pprint
import random
import utils
import myQueue
import time
import matplotlib.pyplot as plt
import os


class Pair:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __key(self):
        return str(self.a)+','+str(self.b)

    def __eq__(x, y):
        return x.a == y.a and x.b == y.b

    def __hash__(self):
        return hash(self.__key())


def func(graph1, graph2, sources, params, stag):
    hardpart = False
    successCounter = 0
    sumGoodDeg, sumBadDeg = 0, 0
    successRate = []
    avgGoodDeg, avgBadDeg, recommends, degree = [], [], [], []
    #print(len(graph1.edges()))
    #print(len(graph2.edges()))
    print("seed: " + str(len(sources)))
    M, unM = {}, {}
    A, Z = set(), set()
    countLoops = 0
    countMapped1degree = 0
    queue = myQueue.MaxQueue()
    for source in sources:
        M[source] = source
        unM[source] = source
        A.add(Pair(source, source))
    while len(A) != 0:
        countLoops += 1
        for pair in A:
            Z.add(pair)
            addMarkToFriends(queue, pair, graph1, graph2, M, unM)
        print(f"end of for {countLoops}. time: {time.time() - start}")
        while (not queue.isEmpty()) and (queue.top()[0] > 0): #################################################################
            pop = queue.pop()
            if pop[0] <= 1000:
                hardpart = True
                queue = reorderQueue(queue, graph1, graph2, M, unM)
            pair = pop[1]
            if M.__contains__(pair.a) or unM.__contains__(pair.b):
                continue
            if pair.a != pair.b and successCounter < stag:
                continue

            recommends.append(Log(pop[0]/1000))
            if not hardpart and len(graph1[pair.a]) == 1:
                countMapped1degree += 1
            M[pair.a] = pair.b
            unM[pair.b] = pair.a
            if len(M) % 500 == 0:
                print("done: " + str(len(M)))
            if not Z.__contains__(pair):
                Z.add(pair)
                addMarkToFriends(queue, pair, graph1, graph2, M, unM)
            if pair.a == pair.b:
                successCounter += 1
                sumGoodDeg += Log(len(graph1[pair.a]))
            else:
                sumBadDeg += Log(len(graph1[pair.a]))
            if hardpart: ####and len(graph1[pair.a]) < 100   hardpart
                degree.append(len(graph1[pair.a]))
            avgGoodDeg.append(sumGoodDeg / successCounter)
            value = 1 if pair.a == pair.b else 0
            successRate.append(value)
            ####successRate.append(successCounter/(len(M) - len(sources)))
            if len(M) - len(sources) - successCounter > 0:
                avgBadDeg.append(sumBadDeg / (len(M) - len(sources) - successCounter))
            else: avgBadDeg.append(0)

        print(f"end of while {countLoops}. time: {time.time() - start}")
        A = set()
        for a in M:
            for graph1Neighbor in graph1[a]:
                if not M.__contains__(graph1Neighbor):
                    b = M[a]
                    for graph2Neighbor in graph2[b]:
                        if not unM.__contains__(graph2Neighbor):
                            if not Z.__contains__(Pair(a, b)):
                                A.add(Pair(graph1Neighbor, graph2Neighbor))
        print(f"end of updating A {countLoops}. time: {time.time() - start}")
        print(f"mapped with 1 degree: {countMapped1degree}")
        os.mkdir(params["plotDir"])
        plt.plot(utils.smood(successRate, 100))
        plt.title(f"local success percent. total of {len(sources) + successCounter} out of {len(M)}")
        plt.savefig(params["plotDir"]+str(stag) + "-1")
        plt.clf()
        plt.plot(recommends)
        plt.title("number of recommends along time")
        plt.savefig(params["plotDir"]+str(stag) + "-2")
        plt.clf()
        plt.plot(avgGoodDeg)
        plt.title("average degree of good mapped node")
        plt.savefig(params["plotDir"]+str(stag) + "-3")
        plt.clf()
        plt.plot(avgBadDeg)
        plt.title("average degree of wrong mapped node")
        plt.savefig(params["plotDir"]+str(stag) + "-4")
        plt.clf()
        plt.plot(betterSee(degree))
        plt.title("degree of all nodes")
        plt.savefig(params["plotDir"] + str(stag) + "-5")
        plt.clf()
    return M

def betterSee(array):
    result = [0]*100
    for num in array:
        if num < 100:
            result[num] += 1
    for i in range(0, len(result)):
        result[i] = Log(result[i])
    return result

def reorderQueue(queue, graph1, graph2, M, unM):
    print("start reorder")
    newQueue = myQueue.MaxQueue()
    while not queue.isEmpty():
        pop = queue.pop()
        pair = pop[1]
        if M.__contains__(pair.a) or unM.__contains__(pair.b):
            continue
        priority = math.ceil(pop[0] / 1000.0) * 1000 + len(graph1[pair.a]) + len(graph2[pair.b]) - abs(len(graph1[pair.a]) - len(graph2[pair.b]))
        newQueue.push(priority, pair)
    print("finish reorder")
    return newQueue


def addMarkToFriends(queue, pair, graph1, graph2, M, unM):
    for graph1Neighbor in graph1[pair.a]:
        if not M.__contains__(graph1Neighbor):
            for graph2Neighbor in graph2[pair.b]:
                if not unM.__contains__(graph2Neighbor):
                    addMark(queue, Pair(graph1Neighbor, graph2Neighbor), graph1, graph2)


def addMark(queue, pair, graph1, graph2):
    a_degree = len(graph1[pair.a])
    b_degree = len(graph2[pair.b])
    diff = abs(a_degree - b_degree)
    deltaDeg = abs(math.log(a_degree + 1) - math.log(b_degree + 1))
    if queue.contains(pair):
        queue.addToPriority(1000, pair)
    else:
        queue.addToPriority(1000 - diff, pair) #/(a_degree+b_degree)**0.5


def evaluate(M):
    print("********")
    print("len(M) = " + str(len(M)))
    count = 0
    for a in M:
        if M[a] == a:
            count += 1
    print(count)

def Log(a):
    return math.log(a+1)

params = json.load(open('config.json'))["gradualProb"]
graph1, graph2, sources, params = utils.generateFileGraphs(params)
pprint.pprint(params)
#for stag in [0, 100, 1000, 10000]:
start = time.time()
M = func(graph1, graph2, sources, params, 0)
print("time: " + str(time.time() - start))
evaluate(M)
