import json
import math
import pprint
import random
import utils
import myQueue
import time


class Pair:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __key(self):
        return str(self.a)+','+str(self.b)

    def __eq__(x, y):
        return x.a == y.a and x.b == y.b

    def __hash__(self):
        return hash(self.__key())


def func(graph1, graph2, sources, params):
    #print(len(graph1.edges()))
    #print(len(graph2.edges()))
    print("seed: " + str(len(sources)))
    M, unM = {}, {}
    A, Z = set(), set()
    countLoops = 0
    queue = myQueue.MappedQueue()
    fight = False
    for source in sources:
        M[source] = source
        unM[source] = source
        A.add(Pair(source, source))
    while len(A) != 0:
        countLoops += 1
        for pair in A:
            Z.add(pair)
            addMarkToFriends(queue, pair, graph1, graph2, M, unM)
        print(f"end of for {countLoops}. time: {time.time() - start}")
        while (not queue.isEmpty()) and (queue.top()[0] > 1000): #################################################################
            pop = queue.pop()
            pair, priority = pop[1], pop[0]
            if M.__contains__(pair.a) or unM.__contains__(pair.b):
                continue
            if not fight and priority < 3000:
                fight = True
                print("happy")
            M[pair.a] = pair.b
            unM[pair.b] = pair.a
            if len(M) % 500 == 0:
                print("done: " + str(len(M)))
            if not Z.__contains__(pair):
                Z.add(pair)
                if fight:
                    addMarkToFriends2(queue, pair, graph1, graph2, M, unM)
                else:
                    addMarkToFriends(queue, pair, graph1, graph2, M, unM)
        print(f"end of while {countLoops}. time: {time.time() - start}")
        A = set()
        for a in M:
            for graph1Neighbor in graph1[a]:
                if not M.__contains__(graph1Neighbor):
                    b = M[a]
                    for graph2Neighbor in graph2[b]:
                        if not unM.__contains__(graph2Neighbor):
                            if not Z.__contains__(Pair(a, b)):
                                A.add(Pair(graph1Neighbor, graph2Neighbor))
        print(f"end of updating A {countLoops}. time: {time.time() - start}")
    return M


def addMarkToFriends(queue, pair, graph1, graph2, M, unM):
    for graph1Neighbor in graph1[pair.a]:
        if not M.__contains__(graph1Neighbor):
            for graph2Neighbor in graph2[pair.b]:
                if not unM.__contains__(graph2Neighbor):
                    addMark(queue, Pair(graph1Neighbor, graph2Neighbor), graph1, graph2)

def addMarkToFriends2(queue, pair, graph1, graph2, M, unM):
    for graph1Neighbor in graph1[pair.a]:
        if not M.__contains__(graph1Neighbor):
            for graph2Neighbor in graph2[pair.b]:
                if not unM.__contains__(graph2Neighbor):
                    addMarkToFriends(queue, Pair(graph1Neighbor, graph2Neighbor), graph1, graph2, M, unM)


def addMark(queue, pair, graph1, graph2):
    a_degree = len(graph1[pair.a])
    b_degree = len(graph2[pair.b])
    diff = abs(a_degree - b_degree)
    if queue.contains(pair):
        queue.addToPriority(1000, pair)
    else:
        queue.addToPriority(1000 - diff, pair) #/(a_degree+b_degree)**0.5


def evaluate(M):
    print("********")
    print("len(M) = " + str(len(M)))
    count = 0
    for a in M:
        if M[a] == a:
            count += 1
    print(count)


params = json.load(open('config.json'))["gradualProb"]
graph1, graph2, sources, params = utils.generateFileGraphs(params)
pprint.pprint(params)
start = time.time()
M = func(graph1, graph2, sources, params)
print("time: " + str(time.time() - start))
evaluate(M)
