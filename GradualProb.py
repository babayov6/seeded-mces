import numpy
import snap
import math
import networkx
import utils
import heapq
import myQueue
import json
import time
import pprint
import random

class myNode:
    def __init__(self, name, mapsProbabilities={}, g=0, fixed=False):
        self.name = name
        self.mapsProbabilities = mapsProbabilities
        self.g = g
        self.fixed = fixed

        def __key(self):
            return self.name

        def __eq__(x, y):
            return isinstance(y, x.__class__) and x.__key() == y.__key()

        def __hash__(self):
            return hash(self.__key())


def main():
    print("main")
    #recalculateHeap = initilize()

def createMyGraph(graph1, sources):
    recalculateQueue = myQueue.MappedQueue()
    myNodesGraph1 = networkx.Graph()
    myNodes = {}
    #build myNodes
    for node in graph1.nodes:
        if node in sources:
            myNodes[node] = myNode(name=node, mapsProbabilities={node: 1}, g=1, fixed=True)
        else:
            myNodes[node] = myNode(name=node)
            recalculateQueue.push(0, myNode)
    #build myNodesGraph1
    for edge in graph1.edges:
        myNodesGraph1.add_edge(myNodes[edge[0]], myNodes[edge[1]])
    sources = [myNodes[source] for source in sources]
    return myNodesGraph1, sources

def initilizeQueue(myNodesGraph1, sources):
    recalculateQueue = myQueue.MaxQueue()
    for node in myNodesGraph1.nodes:
        if node not in sources:
            recalculateQueue.push(0, node)
    for node in sources:
        updateQueue(myNodesGraph1, sources, recalculateQueue, node, prevMapsProbabilities={})
    return recalculateQueue

def updateQueue(myNodesGraph1,sources, recalculateQueue, node, prevMapsProbabilities):
    changeInNode = 0
    for map in prevMapsProbabilities:
        if map in node.mapsProbabilities:
            changeInNode += abs(node.mapsProbabilities[map] - prevMapsProbabilities[map])
        else:
            changeInNode += prevMapsProbabilities[map]
    for map in node.mapsProbabilities:
        if map not in prevMapsProbabilities:
            changeInNode += node.mapsProbabilities[map]

    #apdate priority of each neighbor
    for neighbor in myNodesGraph1[node]:
        if neighbor not in sources:
            newValue = node.g * changeInNode / math.log(len(myNodesGraph1[neighbor])+1) ################### critic math.log
            recalculateQueue.addToPriority(newValue, neighbor)


def normalizeDict3(params, dict):
    count = 0
    max = -1
    for i in dict:
        if dict[i] == max:
            count += 1
        if dict[i] > max:
            count = 1
            max = dict[i]
    newDic = {}
    for i in dict:
        if dict[i] == max:
            newDic[i] = 1/count
    return newDic

def normalizeDict2(params, dict):
    sum = 0
    for i in dict:
        sum += params["normalizePow"]**dict[i]
    for i in dict:
        dict[i] = (params["normalizePow"]**dict[i]) / sum
    return dict

def normalizeDict(dict):
    sum = 0
    for i in dict:
        sum += dict[i]
    for i in dict:
        dict[i] = dict[i] / sum
    return dict

def updateG(myNodesGraph1, graph2, v, MapsProbabilities):
    sum = 0
    for neighbor in myNodesGraph1[v]:
        for possibleMap in MapsProbabilities:
            for neighborPossibleMap in neighbor.mapsProbabilities:
                if(possibleMap in graph2[neighborPossibleMap]):
                    sum += neighbor.g * MapsProbabilities[possibleMap] * neighbor.mapsProbabilities[neighborPossibleMap] #
    return sum / len(myNodesGraph1[v])

def getFriendsDeg(Graph, node):
    sumDegs = 0
    for friend in Graph[node]:
        sumDegs += len(Graph[friend])
    return sumDegs / len(Graph[node])


def check(params, myNodesGraph1, graph2):
    containsMapCount = 0
    firstMapCount = 0
    hasMapCount = 0

    moment1 = 0
    #moment1Target = 0

    for node in myNodesGraph1:
        if bool(node.mapsProbabilities):
            hasMapCount += 1
            #moment1Target += abs(getFriendsDeg(myNodesGraph1, node) - getFriendsDeg(graph2, node.name))
            moment1 += abs(getFriendsDeg(myNodesGraph1, node) - getFriendsDeg(graph2, [*node.mapsProbabilities][0]))
            if node.name in node.mapsProbabilities:
                containsMapCount += 1
                isFirst = True
                for map in node.mapsProbabilities:
                    if node.mapsProbabilities[map] > node.mapsProbabilities[node.name]:
                        isFirst = False
                if isFirst:
                    firstMapCount += 1
    moment1 = moment1 / hasMapCount
    #moment1Target = moment1Target / hasMapCount
    print("moment1= " + str(moment1))
    #print("moment1Target= " + str(moment1Target))
    containsMap = "{:.2f}".format(100*(containsMapCount/params["nodes"] - params["sources"])/(1-params["sources"]))
    firstMap = "{:.2f}".format(100*(firstMapCount/params["nodes"] - params["sources"])/(1-params["sources"]))
    hasMap = "{:.2f}".format(100*(hasMapCount/params["nodes"] - params["sources"])/(1-params["sources"]))
    print("has a map: " + hasMap + "%,  contains right map: " + containsMap + "%,  first map: " + firstMap + "%\n")
    return moment1, hasMap


def myGenerateGraphs(params):
    graph1, graph2, sources = utils.generateGraphs(params)
    myNodesGraph1, sources = createMyGraph(graph1, sources)
    return myNodesGraph1, graph2, sources

def LoopsToPrecent(num, params):
    return 100*num / (params["nodes"]*(1-params["sources"]))

def mainLoop(myNodesGraph1, graph2, sources, params):
    recalculateQueue = initilizeQueue(myNodesGraph1, sources)
    index = 0
    qualityScale = 100
    loop = True
    loopPrecent = LoopsToPrecent(index, params)
    start = time.time()
    while loop:
        if index % 10000 == 0:
        #if loopPrecent != 0 and int(loopPrecent) % 10 == 0:
            if params["extraPrint"] or loopPrecent == params["loopsInNodesPrecents"]:
                sec = int((time.time() - start) / 1)
                print(str(index) + " loops = " + str(int(loopPrecent)) +"%, " + str(sec) + " sec")
                newqualityScale, hasMap = check(params, myNodesGraph1, graph2)
                if float(hasMap) >= 99.5:
                    if newqualityScale >= qualityScale:
                        loop = False
                    qualityScale = newqualityScale

        v = recalculateQueue.pop()[1]
        tempQueue = myQueue.MaxQueue()

        #insert all v's new possible maps to temp queue
        for neighbor in myNodesGraph1[v]:
            for map in neighbor.mapsProbabilities:
                for mapsNeighbor in graph2[map]:
                    tempQueue.addToPriority(neighbor.mapsProbabilities[map]*neighbor.g, mapsNeighbor) #

        prevMapsProbabilities = v.mapsProbabilities
        newMapsProbabilities  = {}

        #take all best possible maps
        for i in range(0, params["possibleMaps"]):
            if tempQueue.isEmpty():
                break
            tempMap = tempQueue.pop()
            newMapsProbabilities[tempMap[1]] = tempMap[0]  #since the queue multiply by -1 . ##### canceld  !!!!!!!!!
        v.mapsProbabilities = normalizeDict2(params, newMapsProbabilities)
        #print(v.name, v.mapsProbabilities)
        v.g = updateG(myNodesGraph1, graph2, v, newMapsProbabilities)
        #sources.append(v)

        updateQueue(myNodesGraph1, sources, recalculateQueue, v, prevMapsProbabilities)
        index += 1
        loopPrecent = LoopsToPrecent(index, params)



params = json.load(open('config.json'))["gradualProb"]
graph1, graph2, sources, params = utils.generateFileGraphs(params)
pprint.pprint(params)
myNodesGraph1, sources = createMyGraph(graph1, sources)
mainLoop(myNodesGraph1, graph2, sources, params)
print("\n")


