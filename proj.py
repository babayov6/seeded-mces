import utils
from os import path
import sys
sys.path.append(path.abspath('../StaticGraphEmbeddings-master/StaticGraphEmbeddings/'))
import our_embeddings_methods.OGRE as OGRE
import our_embeddings_methods.utils as ogreUtils
import our_embeddings_methods.static_embeddings as static_embeddings
import numpy
import networkx
import time
import sklearn.neighbors


def diffProjToOgerProj(proj, sources, nodes):
    dic = {}
    for node in nodes:
        tempArr = []
        for source in sources:
            tempArr.append(proj[source][node])
        dic[node] = tempArr
    return dic


def createKD(g_proj):
    dic = {}
    array = []
    i=0
    for key in g_proj:
        array.append(g_proj[key])
        dic[i] = key
        i+=1
    #return spatial.KDTree(array), dic
    return sklearn.neighbors.KDTree(array), dic


# a function used to count how many of the matches we did,
# whold be the same if we swich between g and h
def muchByTree2(gTree, hTree, gDic, hDic, g_proj, h_proj, regularNodes):
    match = {}
    countTries = 0
    countSuccess = 0
    for node in regularNodes:
        # pairInTree = tree.query(g_proj[node])[1]
        pairInTree = hTree.query([g_proj[node]])[1][0][0]
        pair = hDic[pairInTree]

        pairOfPairInTree = gTree.query([h_proj[pair]])[1][0][0]
        pairOfPair = gDic[pairOfPairInTree]

        if pairOfPair == node:
            countTries += 1
            if node == pair:
                countSuccess += 1
            match[node] = pair
    return countTries, countSuccess

def manipulate(x):
    return 1.0/x


def disToProb(distances):
    result = []
    sumProb = 0.0
    for i in range(0, len(distances)):
        sumProb += manipulate(distances[i])
    for i in range(0, len(distances)):
        result.append(manipulate(distances[i]) / sumProb)
    return result

def muchByDoubleProb(gProbDic, hProbDic):
    count = 0
    count2_3 = 0
    count4_5 = 0
    count6_10 = 0
    count11_20 = 0
    for node in gProbDic:
        a = gProbDic[node]
        if node in hProbDic:
            for friend in hProbDic[node]:
                b = hProbDic[node]
                if friend not in gProbDic[node]:
                    gProbDic[node][friend] = hProbDic[node][friend]
                else:
                    gProbDic[node][friend] = gProbDic[node][friend] + hProbDic[node][friend]
        maxValue = 0
        maxNode = 0
        pairsbyprob = []
        for pair in gProbDic[node]:
            pairsbyprob.append((pair, gProbDic[node][pair]))
            if gProbDic[node][pair] > maxValue:
                maxValue = gProbDic[node][pair]
                maxNode = pair
        pairsbyprob.sort(key=lambda tup: tup[1], reverse=True)
        if (pairsbyprob[0][0] == node):
            count += 1
        if len(pairsbyprob) >= 2:
            myRange = min(3, len(pairsbyprob))
            for i in range(1, myRange):
                if (pairsbyprob[i][0] == node):
                    count2_3 += 1
        if len(pairsbyprob) >= 4:
            myRange = min(5, len(pairsbyprob))
            for i in range(3, myRange):
                if (pairsbyprob[i][0] == node):
                    count4_5 += 1
        if len(pairsbyprob) >= 6:
            myRange = min(10, len(pairsbyprob))
            for i in range(5, myRange):
                if (pairsbyprob[i][0] == node):
                    count6_10 += 1
        if len(pairsbyprob) >= 11:
            myRange = min(20, len(pairsbyprob))
            for i in range(10, myRange):
                if (pairsbyprob[i][0] == node):
                    count11_20 += 1
    print("count2_3= " + str(count2_3/len(gProbDic)))
    print("count4_5= " + str(count4_5/len(gProbDic)))
    print("count6_10= " + str(count6_10 / len(gProbDic)))
    print("count11_20= " + str(count11_20 / len(gProbDic)))
    return count

def doubleProbabilityByTree(gTree, hTree, gDic, hDic, g_proj, h_proj, regularNodes, n):
    gProbDic = {}
    hProbDic = {}
    for node in regularNodes:
        treeResult= hTree.query([g_proj[node]], n)
        closeNodesIndexes = treeResult[1][0]
        closeNodesDis = treeResult[0][0]
        closeNodesProb = disToProb(closeNodesDis)
        nodeDic = {}
        for i in range(0, n):
            nodeDic[hDic[closeNodesIndexes[i]]] = closeNodesProb[i]
        gProbDic[node] = nodeDic

        treeResult = gTree.query([h_proj[node]], n)
        closeNodesIndexes = treeResult[1][0]
        closeNodesDis = treeResult[0][0]
        closeNodesProb = disToProb(closeNodesDis)
        for i in range(0, n):
            friend = gDic[closeNodesIndexes[i]]
            if not friend in hProbDic:
                hProbDic[friend] = {}
            hProbDic[friend][node] = closeNodesProb[i]
    return gProbDic, hProbDic


def muchByTree(tree,g_proj,regularNodes,dic):
    match = {}
    count = 0
    for node in regularNodes:
        #pairInTree = tree.query(g_proj[node])[1]
        pairInTree = tree.query([g_proj[node]])[1][0][0]
        pair = dic[pairInTree]
        match[node] = pair
        if node == pair:
            count += 1
    return count

def Match_N_2(proj1,proj2):
    count = 0
    for node1 in proj1:
        mindis = 10000
        pair = -1
        for node2 in proj2:
            if distance(proj1[node1],proj2[node2]) < mindis :
                mindis = distance(proj1[node1],proj2[node2])
                pair = node2
        if node1 == pair:
            count = count+1
    return count

def distance(a,b):
    sum = 0
    for i in range(0, len(a)):
        sum = sum + (a[i] - b[i])**2
    return sum

def Diffusion(graph, sources_embed, DIFF_ITERATIONS, DIFF_RATE):
    embed = sources_embed
    length = len(next(iter(sources_embed.values())))
    for node in graph.nodes:
        if node not in embed:
            embed[node] = numpy.array([0.0]*length)
    newEmbed = {}
    for i in range(0, DIFF_ITERATIONS):
        for node in graph.nodes:
            newEmbed[node] = embed[node]
            for friend in graph[node]:
                newEmbed[node] = newEmbed[node] + DIFF_RATE * (embed[friend] - embed[node])
        embed = newEmbed.copy()
    return embed

def doOGRE(sources_embed ,graph, sources, regularNodes, epsilon):
    dim = len(next(iter(sources_embed.values())))
    neighbors_dict = ogreUtils.create_dict_neighbors(graph)
    dict_node_node, dict_node_enode, dict_enode_enode = \
        ogreUtils.create_dicts_of_connections(set(sources), set(regularNodes), neighbors_dict)
    a, b = OGRE.final_function_OGRE(sources_embed, dict_node_enode, dict_node_node, dict_enode_enode,
                               set(regularNodes), 0.01, dim, graph, epsilon=epsilon)
    return a, b

def normalBase(sources):
    result = {}
    i=0
    for source in sources:
        temp = numpy.array([0.0]*len(sources))
        temp[i] = 1.0
        result[source] = temp
        i = i+1
    return result

def ogerForSources(graph, sources, dim , epsilon):
    A = static_embeddings.StaticEmbeddings("graph", graph, initial_size=500, initial_method="node2vec", method="OGRE", H=None,
                 dim=dim, choose="degrees", regu_val=0, weighted_reg=False, epsilon=epsilon, file_tags=None)
    sources_embed1 = {}
    for source in sources:
        sources_embed1[source] = A.list_dicts_embedding[0][source]#.tolist()
    return sources_embed1


def alg(g, method, base, ogerDim = 128 ):

    DIFF_RATE = 0.005
    DIFF_ITERATIONS = 6
    OGER_EPSILON = 0.01
    OGER_DIM = ogerDim############
    print(OGER_DIM)
    FalseEdgesPercent = 1.00
    SOURCE_PERCENT = 0.05

    FalseEdges = int(FalseEdgesPercent * g.GetEdges())
    h = utils.duplicate(g)
    g = utils.addRandomEdges(g, FalseEdges)
    h = utils.addRandomEdges(h, FalseEdges)
    sources = utils.chooseSources(g, SOURCE_PERCENT)
    regularNodes = utils.notSources(g, sources)

    net_g = utils.snapToNetworx(g)
    net_h = utils.snapToNetworx(h)
    sources_embed1 = {}
    totalTime = 0
    start = time.time()
    if base == "oger":
        sources_embed1 = ogerForSources(net_g, sources, OGER_DIM, OGER_EPSILON)
    elif base =="normal":
        sources_embed1 = normalBase(sources)
    for source in sources_embed1:
        sources_embed1[source] = sources_embed1[source]*1000
    sources_embed2 = sources_embed1.copy()
    sec = int((time.time() - start) / 1)
    totalTime +=sec
    print("base proj: " + str(sec))

    start = time.time()
    if method == "diffusion":
        g_proj = Diffusion(net_g, sources_embed1, DIFF_ITERATIONS, DIFF_RATE)
        h_proj = Diffusion(net_h, sources_embed2, DIFF_ITERATIONS, DIFF_RATE)
    if method == "oger":
        g_proj, g_n_e = doOGRE(sources_embed1,net_g, sources, regularNodes,OGER_EPSILON)
        h_proj, f_n_e = doOGRE(sources_embed2,net_h, sources, regularNodes,OGER_EPSILON)
    sec = int((time.time() - start) / 1)
    totalTime += sec
    print("projections: " + str(sec))

    g_proj = utils.removeSources(g_proj, sources)
    h_proj = utils.removeSources(h_proj, sources)

    start = time.time()
    hTree, hDic = createKD(h_proj)
    gTree, gDic = createKD(g_proj)
    gProbDic, hProbDic = doubleProbabilityByTree(gTree, hTree, gDic, hDic, g_proj, h_proj, regularNodes, 10)
    count = muchByDoubleProb(gProbDic, hProbDic)
    #count = Match_N_2(g_proj,h_proj)

    #countTries , countSuccess = muchByTree2(gTree, hTree, gDic, hDic, g_proj, h_proj, regularNodes)
    #print("success rate = " + str(countTries) + " " + str(countSuccess))

    #tree, dic = createKD(h_proj)
    #count = muchByTree(tree, g_proj, regularNodes, dic)
    sec = int((time.time() - start)/1)
    totalTime += sec
    print("matching: " + str(sec))
    print("total time:" + str(totalTime))

    print("success rate = " + str(count * 1.0 / len(regularNodes)))
    print("************")
    return count * 1.0 / len(regularNodes)


allFiles = ["200-12K","260-2500","291-3000","380-900","440-1000","472-2800","500-42K"]
num_nodes = [1000,20000,30000,40000]
iterations = 1
dims = [128]
graphType = ["random"] #"cluster",
bases = ["oger", "normal"]
methods = ["oger", "diffusion"]#,]
clique_size = 200
p_in = 0.01 * 2
p_out = 0.0002

avg = 0
#oger, diffusion - oger, normal

for type in graphType:
    print("graph type=" + type)
    for base in bases:
        for method in methods:
            #if base == "oger" and method == "diffusion":
            #    continue
            print("base=" + base + " method=" + method)
            #num = 500
            for num in num_nodes:
                #print("dim = " + str(dim))
                avg = 0
                for i in range(0, iterations):
                    if type == "random":
                        G_src = networkx.powerlaw_cluster_graph(num, m=int(7.5*clique_size * p_in),
                                                                p=1000*p_out * clique_size / num)
                    else:
                        G_src = networkx.gaussian_random_partition_graph(n=num, s=clique_size, v=5,
                                                                  p_in=p_in, p_out=p_out, directed=False)
                    graph = utils.networxToSnap(G_src)
                    print("nodes: " + str(graph.GetNodes()) + " edges " + str(graph.GetEdges()))
                    avg += alg(g=graph, method=method, base=base)
                print("success: " + str(avg/iterations))
                print("____________")
        print("___________________________")