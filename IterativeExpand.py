import json
import pprint
import random
import utils
import myQueue
import time
import math
import pandas as pd
from sklearn import linear_model
import numpy as np
from queue import *
import matplotlib.pyplot as plt
import os
import pickle
import sys


class Plots:
    def __init__(self):
        self.success_rate = []

        self.correct_map_degree = []
        self.wrong_map_degree = []

        self.correct_map_recommends = []
        self.wrong_map_recommends = []
        self.wrong_map_against = []
        self.correct_map_against = []

        self.correct_recommends_percent_b = []
        self.wrong_recommends_percent_b = []
        self.wrong_against_percent_b = []
        self.correct_against_percent_b = []
        self.correct_recommends_percent_y = []
        self.wrong_recommends_percent_y = []
        self.wrong_against_percent_y = []
        self.correct_against_percent_y = []

        self.correct_against_divide_recommends = []
        self.wrong_against_divide_recommends = []
        self.against_divide_recommends = []

        self.correct_map_pos = []
        self.wrong_map_pos = []

        self.correct_delta_degree = []
        self.correct_friend_delta_degree = []
        self.wrong_delta_degree = []
        self.wrong_friend_delta_degree = []


class Data:
    def __init__(self, a):
        self.degree = a
        self.unMatched = a
        self.friendsAvgLogDegree = 0
        self.friendsDeviation = 0


class MyExpandWhenStuck:
    def __init__(self, graph1, graph2, sources, params, nodes_to_match):
        self.graph1 = graph1
        self.graph2 = graph2
        self.sources = sources
        self.params = params
        self.graph1Data = self.extractData(self.graph1)
        self.graph2Data = self.extractData(self.graph2)
        self.plots = Plots()
        self.queue = myQueue.MaxQueue()
        self.againstCounter = {}
        self.M, self.unM = {}, {}
        self.ordered_pairs = []
        self.pairs_dic = {}
        self.recommendsCounter = {}
        self.scoreCounter = {}
        self.nodes_to_match = nodes_to_match
        # file = open("pickle_reggresion", 'rb')
        # self.early_regression = pickle.load(file)
        # file.close()
        # file = open("pickle_std", 'rb')
        # self.early_regression_std = pickle.load(file)
        # file.close()
        # file = open("pickle_avg", 'rb')
        # self.early_regression_mean = pickle.load(file)
        # file.close()
        self.early_regression, self.early_regression_std, self.early_regression_mean = None, None, None
        self.regression = {"pairDegree": [], "deltaDegree": [], "percentDeltaDegree": [], "neighborsDegree": [],
                           "deltaNeighborsDegree": [], "friendsPercentDeltaDegree": [], "deltaStd": [], "std": [],
                           "score": [], "recommends": [], "against": [], "result": [], "place": []}
        self.like_regression = {"pairDegree": [], "deltaDegree": [], "percentDeltaDegree": [], "neighborsDegree": [],
                                "deltaNeighborsDegree": [], "friendsPercentDeltaDegree": [], "deltaStd": [], "std": [],
                                "score": [], "recommends": [], "against": [], "result": [], "place": []}

    def extractData(self, graph):
        data = {}
        for node in graph:
            data[node] = Data(len(graph[node]))
        for node in graph:
            sum = 0
            for friend in graph[node]:
                sum += math.log(data[friend].degree + 1)
            data[node].friendsAvgLogDegree = sum / data[node].degree
        for node in graph:
            sum = 0
            for friend in graph[node]:
                sum += (math.log(data[friend].degree + 1) - data[node].friendsAvgLogDegree) ** 2
            data[node].friendsDeviation = (sum / data[node].degree) ** 0.5
        return data

    def main_loop_3(self, perv_pairs_dic, take_all, keep_add_marks=True):
        threshold = 0 if take_all else 1000
        Z = set()
        countLoops = 0
        hard_part = False
        A = self.loadSources()
        while len(A) != 0:
            countLoops += 1
            prev_queue = myQueue.MaxQueue()
            for key1, dic in perv_pairs_dic.items():
                for key2, pair in dic.items():
                    prev_queue.update(pair.recommends * 1000 - abs(pair.a_degree - pair.b_degree), pair)
            Z = self.add_mark_to_A_friends(A, Z, countLoops)
            # while (not prev_queue.isEmpty()) and prev_queue.top()[
            #     0] > 1000:
            #     pop = prev_queue.pop()
            while ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or (
                    (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold):
                if self.queue.isEmpty():
                    pop = prev_queue.pop()
                elif prev_queue.isEmpty():
                    pop = self.queue.pop()
                elif prev_queue.top()[0] > self.queue.top()[0]:
                    pop = prev_queue.pop()
                else:
                    pop = self.queue.pop()

                pair = pop[1]
                if pair.a in self.M or pair.b in self.unM:
                    continue
                if keep_add_marks:
                    Z = self.add_pair_to_map(pair, Z, hard_part)
                else:
                    self.M[pair.a] = pair.b
                    self.unM[pair.b] = pair.a
            print(f"end of while {countLoops}. time: {time.time() - start}")
            A = self.update_A(Z, countLoops)
        return self.M

    def main_loop(self):
        Z = set()
        countLoops = 0
        hard_part = False
        A = self.loadSources()
        while len(A) != 0:
            countLoops += 1
            Z = self.add_mark_to_A_friends(A, Z, countLoops)
            while (not self.queue.isEmpty()) and self.queue.top()[
                0] > 1000:  # and len(self.M) < 12232: ################################    len(self.M) < 12232
                pop = self.queue.pop()
                pair = pop[1]
                if pair.a in self.M or pair.b in self.unM:
                    continue
                Z = self.add_pair_to_map(pair, Z, hard_part)
            print(f"end of while {countLoops}. time: {time.time() - start}")
            A = self.update_A(Z, countLoops)
        return self.M

    def addMarkToFriends(self, recomending_pair, hard_part):
        for graph1Neighbor in graph1[recomending_pair.a]:
            # if not self.M.__contains__(graph1Neighbor):
            for graph2Neighbor in graph2[recomending_pair.b]:
                # if not self.unM.__contains__(graph2Neighbor):
                # points = abs(data1[graph1Neighbor].friendsAvgDegree - data2[graph2Neighbor].friendsAvgDegree)
                pair_to_update = utils.Pair(graph1Neighbor, graph2Neighbor)
                self.addMark(pair_to_update, recomending_pair, hard_part)

    def addMark(self, pair, recommendPair, hard_part):
        simple_delta_deg = abs(self.graph1Data[pair.a].degree - self.graph2Data[pair.b].degree)

        if pair.a not in self.pairs_dic:
            self.pairs_dic[pair.a] = {}
        if pair.b not in self.pairs_dic[pair.a]:
            self.initialize_pair_values(pair)
            self.pairs_dic[pair.a][pair.b] = pair
        else:
            pair = self.pairs_dic[pair.a][pair.b]
        self.update_pair_values(pair, recommendPair)

        if False and hard_part:
            self.queue.update(self.ML_priority(pair), pair)
        elif self.queue.contains(pair):
            self.queue.addToPriority(1000, pair)
        else:
            self.queue.addToPriority(1000 - simple_delta_deg, pair)  # - 2*friendsDeltaDeg-deltaDeg

    def reorderQueue(self):
        print("start reorder")
        new_queue = myQueue.MaxQueue()
        while not self.queue.isEmpty():
            pop = self.queue.pop()
            pair = pop[1]
            if self.M.__contains__(pair.a) or self.unM.__contains__(pair.b):
                continue
            self.initialize_pair_values(pair)
            self.update_pair_against_percent_b(pair)
            new_queue.update(self.hard_part_priority(pair), pair)
        print("finish reorder")
        self.queue = new_queue

    def ML_priority(self, pair):
        data = self.data_to_predict_regression(pair)
        for i in range(0, len(data)):
            data[i] = (data[i] - self.early_regression_mean[i]) / self.early_regression_std[i]
        # temp = self.early_regression.predict([data])[0]
        temp = self.early_regression.predict_proba([data])[0][1]
        # if temp == 0:
        #    print(temp)
        return temp

    def hard_part_priority(self, pair):
        # pair.against = self.countAgainst(pair) + 1
        # return self.new_hard_part_priority(pair)
        numerator = pair.adamicAdar * pair.recommends
        denominator = pair.against * pair.percentDeltaLogDegree ** 2 * pair.friendsPercentDeltaLogDegree
        return numerator / denominator

    def new_hard_part_priority(self, pair):
        stdA = self.graph1Data[pair.a].friendsDeviation
        stdB = self.graph2Data[pair.b].friendsDeviation
        values = [pair.log_degree_sum, pair.deltaLogDegree, pair.percentDeltaLogDegree, pair.friendsLogDegreeSum,
                  pair.friendsDeltaLogDegree,
                  pair.friendsPercentDeltaLogDegree, pair.recommends, pair.against, abs(stdA - stdB), stdA + stdB,
                  pair.adamicAdar]
        np_values = np.array(values)
        normilized = (np_values - self.early_regression_mean) / self.early_regression_std
        return_value = (normilized * self.early_regression).sum()
        # print(return_value)
        return return_value

    def initialize_pair_values(self, pair):
        pair.a_degree = len(self.graph1[pair.a])
        pair.b_degree = len(self.graph2[pair.b])
        degreeA = math.log(pair.a_degree + 1)
        degreeB = math.log(pair.b_degree + 1)
        pair.log_degree_sum = degreeB + degreeA
        pair.deltaLogDegree = abs(degreeA - degreeB)
        pair.percentDeltaLogDegree = (2 * pair.deltaLogDegree + 1) / pair.log_degree_sum

        neighborsDegreeA = self.graph1Data[pair.a].friendsAvgLogDegree
        neighborsDegreeB = self.graph2Data[pair.b].friendsAvgLogDegree
        pair.friendsLogDegreeSum = neighborsDegreeA + neighborsDegreeB
        pair.friendsDeltaLogDegree = abs(neighborsDegreeA - neighborsDegreeB)
        pair.friendsPercentDeltaLogDegree = (2 * pair.friendsDeltaLogDegree + 1) / pair.friendsLogDegreeSum
        # pair.against = self.countAgainst(pair) + 1

    def update_pair_values(self, pair, recommend_pair):
        pair.recommends += 1
        pair.second_recommends += recommend_pair.recommends

        temp1 = math.log(len(self.graph1[recommend_pair.a]) + 1)
        temp2 = math.log(len(self.graph2[recommend_pair.b]) + 1)
        score = 1 / (temp1 + temp2)
        pair.adamicAdar += score
        self.update_pair_against_percent_b(pair)

    def update_pair_against_percent_b(self, pair):
        pair.recommend_percent = pair.recommends / (pair.a_degree + pair.b_degree)

        mapped_a_friends = pair.a_degree - self.graph1Data[pair.a].unMatched
        mapped_b_friends = pair.b_degree - self.graph2Data[pair.b].unMatched

        pair.against = mapped_a_friends + mapped_b_friends - 2 * pair.recommends + 1
        pair.against_percent = pair.against / (pair.a_degree + pair.b_degree)

    def update_pair_against_percent_y(self, pair):
        pair.recommend_percent = pair.recommends ** 2 / (len(self.graph1[pair.a]) * len(self.graph2[pair.b]))

        mapped_a_friends = len(self.graph1[pair.a]) - self.graph1Data[pair.a].unMatched
        mapped_b_friends = len(self.graph2[pair.b]) - self.graph2Data[pair.b].unMatched

        pair.against = mapped_a_friends * mapped_b_friends - pair.recommends ** 2 + 1
        pair.against_percent = pair.against / (len(self.graph1[pair.a]) * len(self.graph2[pair.b]))

    def data_to_predict_regression(self, pair):
        array = []
        array.append(pair.log_degree_sum)
        array.append(pair.deltaLogDegree)
        array.append(pair.percentDeltaLogDegree)

        array.append(pair.friendsLogDegreeSum)
        array.append(pair.friendsDeltaLogDegree)
        array.append(pair.friendsPercentDeltaLogDegree)

        stdA = self.graph1Data[pair.a].friendsDeviation
        stdB = self.graph2Data[pair.b].friendsDeviation
        array.append(abs(stdA - stdB))
        array.append(stdA + stdB)

        array.append(pair.adamicAdar)
        array.append(pair.recommends)

        self.update_pair_against_percent_b(pair)
        array.append(pair.against)
        array.append(len(self.M))
        return array

    def addToregression(self, pair, regression):  # , recommendPair, against
        regression["pairDegree"].append(pair.log_degree_sum)
        regression["deltaDegree"].append(pair.deltaLogDegree)
        regression["percentDeltaDegree"].append(pair.percentDeltaLogDegree)

        regression["neighborsDegree"].append(pair.friendsLogDegreeSum)
        regression["deltaNeighborsDegree"].append(pair.friendsDeltaLogDegree)
        regression["friendsPercentDeltaDegree"].append(pair.friendsPercentDeltaLogDegree)

        stdA = self.graph1Data[pair.a].friendsDeviation
        stdB = self.graph2Data[pair.b].friendsDeviation
        regression["deltaStd"].append(abs(stdA - stdB))
        regression["std"].append(stdA + stdB)

        regression["score"].append(pair.adamicAdar)
        regression["recommends"].append(pair.recommends)

        # against = self.countAgainst(pair)
        self.update_pair_against_percent_b(pair)
        regression["against"].append(pair.against)

        result = 100 if pair.a == pair.b else 0
        regression["result"].append(result)
        regression["place"].append(len(self.M))

    def countAgainst(self, pair):
        # return random.uniform(0, 1)
        against = 0
        for friend in self.graph1[pair.a]:
            if self.M.__contains__(friend) and pair.b not in self.graph2[self.M[friend]]:
                against += 1
        for friend in self.graph2[pair.b]:
            if self.unM.__contains__(friend) and pair.a not in graph1[self.unM[friend]]:
                against += 1
        return against

    def loadSources(self):
        A = set()
        for source in self.sources:
            self.M[source] = source
            self.unM[source] = source
            A.add(utils.Pair(source, source))
        return A

    def add_mark_to_A_friends(self, A, Z, countLoops):
        for pair in A:
            self.addMarkToFriends(pair, False)
            Z.add(pair)
        print(f"finish add_mark_to_A_friends {countLoops}. time: {time.time() - start}")
        return Z

    def update_A(self, Z, countLoops):
        A = set()
        for a in self.M:
            b = self.M[a]
            for graph1Neighbor in self.graph1[a]:
                if graph1Neighbor not in self.M:
                    for graph2Neighbor in self.graph2[b]:
                        if graph2Neighbor not in self.unM:
                            # if utils.Pair(a, self.M[a]) not in Z:
                            if utils.Pair(graph1Neighbor, graph2Neighbor) not in Z:
                                A.add(utils.Pair(graph1Neighbor, graph2Neighbor))
        print(f"end of updating A {countLoops}. time: {time.time() - start}")
        return A

    def add_pair_to_map(self, pair, Z, hard_part):
        self.M[pair.a] = pair.b
        self.unM[pair.b] = pair.a
        self.update_plots(pair)
        self.ordered_pairs.append(pair)
        if len(self.M) % 500 == 0:
            print("done: " + str(len(self.M)))
        if not Z.__contains__(pair):
            self.addMarkToFriends(pair, hard_part)
            Z.add(pair)
        return Z

    def special_remove(self, pair):
        if len(self.plots.against_divide_recommends) <= 300:
            return False
        count = 0
        self.update_pair_against_percent_b(pair)
        val = pair.against_percent / pair.recommend_percent
        for i in range(0, 300):
            if val > self.plots.against_divide_recommends[-1 * i]:
                count += 1
        if count > 285:
            self.plots.against_divide_recommends.append(val)
            return True
        return False

    def update_plots(self, pair, pos=-1):
        val = 1 if pair.a == pair.b else 0
        if pos == -1:
            pos = len(self.M) - len(self.sources)
        self.plots.success_rate.append(val)
        self.update_pair_against_percent_b(pair)
        self.plots.against_divide_recommends.append(pair.against_percent / pair.recommend_percent)

        if val == 1:
            self.plots.correct_map_pos.append(pos)
            self.plots.correct_map_against.append(pair.against)
            self.plots.correct_map_degree.append(pair.log_degree_sum / 2)
            self.plots.correct_map_recommends.append(pair.recommends)
            self.plots.correct_recommends_percent_b.append(pair.recommend_percent)
            self.plots.correct_against_percent_b.append(pair.against_percent)
            self.plots.correct_against_divide_recommends.append(pair.against_percent / pair.recommend_percent)
            self.plots.correct_delta_degree.append(pair.percentDeltaLogDegree)
            self.plots.correct_friend_delta_degree.append(pair.friendsPercentDeltaLogDegree)
        else:
            self.plots.wrong_map_pos.append(pos)
            self.plots.wrong_map_against.append(pair.against)
            self.plots.wrong_map_degree.append(pair.log_degree_sum / 2)
            self.plots.wrong_map_recommends.append(pair.recommends)
            self.plots.wrong_recommends_percent_b.append(pair.recommend_percent)
            self.plots.wrong_against_percent_b.append(pair.against_percent)
            self.plots.wrong_against_divide_recommends.append(pair.against_percent / pair.recommend_percent)
            self.plots.wrong_delta_degree.append(pair.percentDeltaLogDegree)
            self.plots.wrong_friend_delta_degree.append(pair.friendsPercentDeltaLogDegree)

        self.update_pair_against_percent_y(pair)
        if val == 1:
            self.plots.correct_recommends_percent_y.append(pair.recommend_percent)
            self.plots.correct_against_percent_y.append(pair.against_percent)
        else:
            self.plots.wrong_recommends_percent_y.append(pair.recommend_percent)
            self.plots.wrong_against_percent_y.append(pair.against_percent)
        self.update_pair_against_percent_b(pair)

    def save_plots(self, global_success_num, dir_name, use_smooth=False):
        directory = self.params["plotDir"] + dir_name + "/"
        smooth = self.params["smooth"] if use_smooth else 1
        os.mkdir(directory)
        good_pos = self.plots.correct_map_pos[0:len(utils.smood(self.plots.correct_map_pos, smooth))]
        bad_pos = self.plots.wrong_map_pos[0:len(utils.smood(self.plots.wrong_map_pos, smooth))]

        plt.plot(utils.smood(self.plots.success_rate, 100))
        plt.title(f"Sliding window of success. total of {global_success_num} out of {len(self.M)}")
        plt.savefig(directory + "success percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_map_degree, smooth), label='Correct')
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_degree, smooth), label='Wrong')
        plt.title("Log degree of mapped pairs")
        plt.legend(loc="best")
        plt.savefig(directory + "log degree")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_map_recommends, smooth), label='Recommendations of correct',
                 alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_recommends, smooth), label='Recommendations of wrong',
                 alpha=0.7)
        plt.title("Recommendations of mapped pairs")
        plt.legend()
        plt.savefig(directory + "Recommendations")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_map_against, smooth), label='Discouragements of correct',
                 alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_against, smooth), label='Discouragements of wrong',
                 alpha=0.7)
        plt.title("Discouragements of mapped pairs")
        plt.legend()
        plt.savefig(directory + "Discouragements")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_delta_degree, smooth), label='Delta degree of correct pairs',
                 alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_delta_degree, smooth), label='Delta degree of wrong pairs',
                 alpha=0.7)
        plt.title("Delta degree of mapped pairs in percents")
        plt.legend()
        plt.savefig(directory + "delta degree")
        plt.clf()

        plt.plot(bad_pos, utils.smood(self.plots.wrong_friend_delta_degree, smooth),
                 label='Friends_delta_degree of wrong pairs', alpha=0.7)
        plt.plot(good_pos, utils.smood(self.plots.correct_friend_delta_degree, smooth),
                 label='Friends_delta-degree_of correct pairs', alpha=0.7)
        plt.title("Delta average degree of mapped pairs' neighbors in percents")
        plt.legend()
        plt.savefig(directory + "delta neighbors degree")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_recommends_percent_b, smooth),
                 label='Recommendations percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_recommends_percent_b, smooth),
                 label='Recommendations percent of wrong', alpha=0.7)
        plt.title("Barak-Recommendations percent of mapped pairs")
        plt.legend()
        plt.savefig(directory + "Barak-recommends_percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_against_percent_b, smooth),
                 label='Discouragements percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_against_percent_b, smooth),
                 label='Discouragements percent of wrong', alpha=0.7)
        plt.title("Barak-Discouragements percent of mapped pairs")
        plt.legend()
        plt.grid()
        plt.savefig(directory + "Barak-discouragements_percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_against_divide_recommends, smooth),
                 label='correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_against_divide_recommends, smooth),
                 label='wrong', alpha=0.7)
        plt.title("Barak-discouragements/recommends percent of mapped pairs")
        plt.legend()
        plt.grid()
        plt.savefig(directory + "Barak-discouragements divide by recommends")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_against_percent_y, smooth),
                 label='Discouragements percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_against_percent_y, smooth),
                 label='Discouragements percent of wrong', alpha=0.7)
        plt.title("Discouragements percent of mapped pairs - Yoram")
        plt.legend()
        plt.savefig(directory + "Yoram-discouragements_percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_recommends_percent_y, smooth),
                 label='Recommendations percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_recommends_percent_y, smooth),
                 label='Recommendations percent of wrongt', alpha=0.7)
        plt.title("Recommendations percent of mapped pairs - Yoram")
        plt.legend()
        plt.savefig(directory + "Yoram-recommends_percent")
        plt.clf()

    def analyzeRegression(self, regression_data):
        # print("***analyzeRegression***")
        std_array = []
        mean_array = []
        keys = [key for key in regression_data.keys()]
        for key in keys:
            if key == "result":
                continue
            array = np.array(regression_data[key])
            avg = array.sum() / array.size
            std = array.std()
            regression_data[key] = ((array - avg) / std).tolist()

            std_array.append(std)
            mean_array.append(avg)
            # array3 = array**3
            # avg3 = array3.sum() / array3.size
            # std3 = array3.std()
            # self.regression[key+"3"] = ((array3 - avg3) / std3).tolist()

        self.early_regression_std = np.array(std_array)
        self.early_regression_mean = np.array(mean_array)

        df = pd.DataFrame(regression_data,
                          columns=["pairDegree", "deltaDegree", "percentDeltaDegree", "neighborsDegree",
                                   "deltaNeighborsDegree",
                                   "friendsPercentDeltaDegree", "deltaStd", "std", "score", "recommends", "against",
                                   "place",
                                   # "pairDegree3", "deltaDegree3", "neighborsDegree3", "deltaNeighborsDegree3",
                                   # "recommends3", "score3", "deltaStd3", "std3", "against3",
                                   "result"])

        X = df[["pairDegree", "deltaDegree", "percentDeltaDegree", "neighborsDegree", "deltaNeighborsDegree",
                "friendsPercentDeltaDegree", "deltaStd", "std", "score", "recommends", "against", "place"]]
        # "pairDegree3", "deltaDegree3", "neighborsDegree3", "deltaNeighborsDegree3", "score3", "recommends3", "against3"]]
        Y = df["result"]
        regression = linear_model.LogisticRegression(penalty='l1',
                                                     solver='liblinear')  # LogisticRegression(penalty='l1', solver='liblinear')  #LinearRegression()
        regression.fit(X, Y)
        file = open("pickle_reggresion", 'wb')
        pickle.dump(regression, file)
        file.close()
        file = open("pickle_std", 'wb')
        pickle.dump(std_array, file)
        file.close()
        file = open("pickle_avg", 'wb')
        pickle.dump(mean_array, file)
        file.close()

        print('Intercept: \n', regression.intercept_)
        print('Coefficients: \n', regression.coef_)

    def evaluate(self, s_f1, s_recall, s_precision):
        print("**evaluate**")
        countCorrect = 0
        correct, wrong = Data(0), Data(0)
        correct_delta_degree = 0
        wrong_delta_degree = 0
        for a in self.M:
            b = self.M[a]
            if a == b:
                countCorrect += 1
        print(f"should match: {self.nodes_to_match}")
        print(f"tried: {len(self.M)}")
        print(f"correct: {countCorrect}")
        precision = countCorrect / len(self.M)
        recall = countCorrect / self.nodes_to_match
        print(f"precision: {100 * precision}")
        print(f"recall: {100 * recall}")
        f1 = 200 * recall * precision / (recall + precision)
        print(f"f1 = {f1}")
        count_edges = 0
        for v in self.graph1:
            if v not in self.M:
                continue
            for u in self.graph1[v]:
                if u in self.M and self.M[v] in self.graph2[self.M[u]]:
                    count_edges += 1

        graph1_edges = 0
        for v in self.graph1:
            if v in self.M:
                graph1_edges += len(self.graph1[v])
        graph2_edges = 0
        for u in self.graph2:
            if u in self.unM:
                graph2_edges += len(self.graph2[u])
        # semi_precision = 200 * count_edges / (graph1_edges + graph2_edges)
        semi_precision = count_edges / len(self.M)
        print(f"semi precision: {semi_precision}")
        print(f"num of correct edges = {count_edges}")
        print("***************\n")
        s_f1.append(round(f1, 2))
        s_recall.append(round(recall * 100, 2))
        s_precision.append(round(precision * 100, 2))
        return countCorrect, count_edges, semi_precision


params = json.load(open('config.json'))["gradualProb"]
# random.seed(1000)
# print(params["plotDir"])
# os.mkdir(params["plotDir"])
graph_num = params["correntGraph"]
params["sources"] = params["relevantSeeds"][graph_num]
params["path"] = params["relevantGraphs"][graph_num]
original_seed = params["sources"]
for seed in [16]:  # [1, 0.5, 0.75, 1, 1.25]:
    seed_f1, seed_recall, seed_precision, seed_count_edges, seed_semi_precision = [], [], [], [], []
    params["sources"] = int(seed * original_seed)
    s_array = [0.4]
    for s in s_array:
        params["falseEdges"] = s
        s_f1, s_recall, s_precision, s_count_edges, s_semi_precision = [], [], [], [], []
        graph1, graph2, sources, params, nodes_to_match = utils.generateFileGraphs(params)
        # graph1, graph2, sources = utils.generateGraphs(params)
        pprint.pprint(params)
        start = time.time()
        myExpandWhenStuck = MyExpandWhenStuck(graph1, graph2, sources, params, nodes_to_match)
        myExpandWhenStuck.main_loop()
        countCorrect, count_edges, semi_precision = myExpandWhenStuck.evaluate(s_f1, s_recall, s_precision)
        s_semi_precision.append(semi_precision)
        s_count_edges.append(count_edges)
        last_count_edges = 0
        last_run_recomends = myExpandWhenStuck.pairs_dic

        count_loops = 1
        noisy_loop, after_noisy_loop, regular_loop = False, False, True
        noisy_loop_num = 0
        while False and (noisy_loop or after_noisy_loop or regular_loop):
            print(f"last ratio of edges: {count_edges / (1 + last_count_edges)}")
            last_count_edges = count_edges
            if noisy_loop:
                noisy_loop_num = count_loops
                print("noisy loop")
            print(f"loop = {count_loops}\n")

            third_myExpandWhenStuck = MyExpandWhenStuck(graph1, graph2, sources, params, nodes_to_match)
            third_myExpandWhenStuck.main_loop_3(last_run_recomends, noisy_loop)
            countCorrect, count_edges, semi_precision = third_myExpandWhenStuck.evaluate(s_f1, s_recall, s_precision)
            s_semi_precision.append(semi_precision)
            s_count_edges.append(count_edges)
            count_loops += 1
            last_run_recomends = third_myExpandWhenStuck.pairs_dic
            if noisy_loop:
                noisy_loop, after_noisy_loop, regular_loop, = False, True, False
            elif last_count_edges * 1.04 < count_edges or count_edges < 0.97 * last_count_edges:
                noisy_loop, after_noisy_loop, regular_loop, = False, False, True
            elif noisy_loop_num == 0:
                noisy_loop, after_noisy_loop, regular_loop, = True, False, False
            else:
                noisy_loop, after_noisy_loop, regular_loop = False, False, False

        print(f"S = {s}, seed={int(seed * original_seed)}:")
        s_f1.append(noisy_loop_num)
        print(s_f1)
        seed_count_edges.append(s_count_edges)
        seed_semi_precision.append(s_semi_precision)
        seed_f1.append(s_f1)
        seed_recall.append(s_recall)
        seed_precision.append(s_precision)
        print(f"time: {str(time.time() - start)}\n\n")

    print(f"seed = {int(seed * original_seed)}:")
    for i in range(len(s_array)):
        print(f"\"{s_array[i]}\": [")
        print(str(seed_count_edges[i]) + ",")
        print(str(seed_semi_precision[i]) + ",")
        print(str(seed_precision[i]) + ",")
        print(str(seed_recall[i]) + ",")
        last_comma = "" if i == len(s_array) - 1 else ","
        print(str(seed_f1[i]) + "]" + last_comma)
        print("\n\n")
