import json
import pprint
import random
import utils
import myQueue
import time
import math
import pandas as pd
from sklearn import linear_model
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
import threading
import functools
import sys


# import ThinIterativ_works as works


class Pair:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.recommends = 0
        self.log_degree_sum = 0
        self.degree_sum = 0

    def __key(self):
        return str(self.a) + ',' + str(self.b)

    def __eq__(self, other):
        return (self.a == other.a) and (self.b == other.b)

    def __hash__(self):
        return hash(self.__key())


class Plots:
    def __init__(self):
        self.success_rate = []

        self.correct_map_degree = []
        self.wrong_map_degree = []
        self.correct_map_log_degree = []
        self.wrong_map_log_degree = []

        self.correct_map_recommends = []
        self.wrong_map_recommends = []
        self.wrong_map_against = []
        self.correct_map_against = []

        self.correct_recommends_percent_b = []
        self.wrong_recommends_percent_b = []
        self.wrong_against_percent_b = []
        self.correct_against_percent_b = []
        self.correct_recommends_percent_y = []
        self.wrong_recommends_percent_y = []
        self.wrong_against_percent_y = []
        self.correct_against_percent_y = []

        self.correct_against_divide_recommends = []
        self.wrong_against_divide_recommends = []
        self.against_divide_recommends = []

        self.correct_map_pos = []
        self.wrong_map_pos = []

        self.correct_delta_degree = []
        self.correct_friend_delta_degree = []
        self.wrong_delta_degree = []
        self.wrong_friend_delta_degree = []


class Data:
    def __init__(self, a):
        self.degree = a
        self.unMatched = a
        self.friendsAvgLogDegree = 0
        self.friendsDeviation = 0


def generic_plot(x, y, title, file_name, eps):
    # plt.title(title, fontdict={'fontsize': 22, 'fontname': 'Times New Roman'}) #
    plt.xlabel(x, fontsize=18, fontname="Times New Roman")
    plt.ylabel(y, fontsize=18, fontname="Times New Roman")
    plt.legend(loc="best")
    # plt.show()
    plt.savefig(file_name, format='eps')
    plt.clf()


class MyExpandWhenStuck:
    def __init__(self, graph1, graph2, sources, params, nodes_to_match):
        self.graph1 = graph1
        self.graph2 = graph2
        self.sources = sources
        self.params = params
        # self.graph1Data = self.extractData(self.graph1)
        # self.graph2Data = self.extractData(self.graph2)
        self.plots = Plots()
        self.queue = myQueue.MaxQueue()
        self.againstCounter = {}
        self.M, self.unM = {}, {}
        self.ordered_pairs = []
        self.pairs_dic = {}
        self.recommendsCounter = {}
        self.scoreCounter = {}
        self.nodes_to_match = nodes_to_match
        # file = open("pickle_reggresion", 'rb')
        # self.early_regression = pickle.load(file)
        # file.close()
        # file = open("pickle_std", 'rb')
        # self.early_regression_std = pickle.load(file)
        # file.close()
        # file = open("pickle_avg", 'rb')
        # self.early_regression_mean = pickle.load(file)
        # file.close()
        self.early_regression, self.early_regression_std, self.early_regression_mean = None, None, None
        self.regression = {"pairDegree": [], "deltaDegree": [], "percentDeltaDegree": [], "neighborsDegree": [],
                           "deltaNeighborsDegree": [], "friendsPercentDeltaDegree": [], "deltaStd": [], "std": [],
                           "score": [], "recommends": [], "against": [], "result": [], "place": []}
        self.like_regression = {"pairDegree": [], "deltaDegree": [], "percentDeltaDegree": [], "neighborsDegree": [],
                                "deltaNeighborsDegree": [], "friendsPercentDeltaDegree": [], "deltaStd": [], "std": [],
                                "score": [], "recommends": [], "against": [], "result": [], "place": []}

    def extractData(self, graph):
        data = {}
        for node in graph:
            data[node] = Data(len(graph[node]))
        for node in graph:
            sum = 0
            for friend in graph[node]:
                sum += math.log(data[friend].degree + 1)
            data[node].friendsAvgLogDegree = sum / data[node].degree
        for node in graph:
            sum = 0
            for friend in graph[node]:
                sum += (math.log(data[friend].degree + 1) - data[node].friendsAvgLogDegree) ** 2
            data[node].friendsDeviation = (sum / data[node].degree) ** 0.5
        return data

    def infinity_loop(self, perv_pairs_dic):
        A = self.loadSources()
        prev_queue = myQueue.MaxQueue()
        for key1, dic in perv_pairs_dic.items():
            for key2, pair in dic.items():
                val = abs(len(self.graph1[pair.a]) - len(self.graph2[pair.b]))
                prev_queue.update(pair.recommends * 1000 - val, pair)
        while (not prev_queue.isEmpty()) and prev_queue.top()[0] > 1000:
            pair = prev_queue.pop()[1]
            if pair.a not in self.M and pair.b not in self.unM:
                self.M[pair.a] = pair.b
                self.unM[pair.b] = pair.a

    def main_loop_3(self, perv_pairs_dic, take_all, keep_add_marks=True):
        threshold = 0 if take_all else 1000
        Z = set()
        countLoops = 0
        hard_part = False
        A = self.loadSources()
        prev_queue = myQueue.MaxQueue()
        for key1, dic in perv_pairs_dic.items():
            for key2, pair in dic.items():
                val = abs(len(self.graph1[pair.a]) - len(self.graph2[pair.b]))
                prev_queue.update(pair.recommends * 1000 - val, pair)
        Z = self.add_mark_to_A_friends_parallel(A, Z, countLoops, first=True)
        while ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or (
                (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold):
            if self.queue.isEmpty():
                pop = prev_queue.pop()
            elif prev_queue.isEmpty():
                pop = self.queue.pop()
            elif prev_queue.top()[0] > self.queue.top()[0]:
                pop = prev_queue.pop()
            else:
                pop = self.queue.pop()

            pair = pop[1]
            if pair.a in self.M or pair.b in self.unM:
                continue
            if keep_add_marks:
                Z = self.add_pair_to_map(pair, Z, hard_part)
                # Z = works.MyExpandWhenStuck.add_pair_to_map(self, pair, Z, hard_part)
            else:
                self.M[pair.a] = pair.b
                self.unM[pair.b] = pair.a
        print(f"end of while {countLoops}. time: {time.time() - start}")

    def iterative_loop(self, perv_pairs_dic, take_all, keep_add_marks=True):
        time_start_loop = time.time()
        threshold = 0 if take_all else 1000
        Z = set()
        last_maps = []
        countLoops = 0
        hard_part = False
        A = self.loadSources()
        prev_queue = myQueue.MaxQueue()
        for key1, dic in perv_pairs_dic.items():
            for key2, pair in dic.items():
                val = abs(len(self.graph1[pair.a]) - len(self.graph2[pair.b]))
                prev_queue.update(pair.recommends * 1000 - val, pair)
        # while len(A) != 0:
        Z = self.add_mark_to_A_friends_parallel(A, Z, countLoops, first=True)
        queue_still_interesting = ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or (
                (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold)
        while ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or (
                (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold):
            # while queue_still_interesting or len(last_maps) > 0:
            # if not queue_still_interesting or len(last_maps) == self.params["parallel_batch"]:
            #     self.update_queue_parallel(last_maps)
            #     last_maps = []
            #     queue_still_interesting = ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or (
            #             (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold)
            #     continue
            if self.queue.isEmpty():
                pop = prev_queue.pop()
            elif prev_queue.isEmpty():
                pop = self.queue.pop()
            elif prev_queue.top()[0] > self.queue.top()[0]:
                pop = prev_queue.pop()
            else:
                pop = self.queue.pop()
            pair = pop[1]
            if pair.a in self.M or pair.b in self.unM:
                continue
            Z = self.add_pair_to_map(pair, Z, hard_part)
            # self.M[pair.a] = pair.b
            # self.unM[pair.b] = pair.a
            # if pair not in Z:
            #     last_maps.append(Pair(pair.a, pair.b))
            #     Z.add(pair)
            # queue_still_interesting = ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or (
            #         (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold)

        print(f"end of loop. time: {time.time() - start}. took {round(time.time() - time_start_loop, 1)} sec")
        # A = self.update_A(Z, countLoops)
        return self.M

    def iterative_loop_parallel(self, perv_pairs_dic, noisy_loop, keep_add_marks=False):
        time_start_loop = time.time()
        print(f"start of loop. time: {time.time() - start}")
        threshold = 0 if noisy_loop else 1000
        Z = set()
        hard_part = False
        A = self.loadSources()
        prev_queue = myQueue.MaxQueue()
        for key1, dic in perv_pairs_dic.items():
            for key2, pair in dic.items():
                val = abs(len(self.graph1[pair.a]) - len(self.graph2[pair.b]))
                prev_queue.update(pair.recommends * 1000 - val, pair)
        # Z = self.add_mark_to_A_friends(A, Z, countLoops)
        while (not prev_queue.isEmpty()) and prev_queue.top()[0] > threshold:
            pop = prev_queue.pop()
            pair = pop[1]
            if pair.a in self.M or pair.b in self.unM:
                continue
            # if keep_add_marks:
            #     Z = self.add_pair_to_map(pair, Z, hard_part)
            else:
                self.M[pair.a] = pair.b
                self.unM[pair.b] = pair.a
        pairs_arry = [Pair(a, self.M[a]) for a in self.M]
        self.pairs_dic = self.parallel(params["threads"], pairs_arry)
        print(f"end of loop. time: {time.time() - start}. took {round(time.time() - time_start_loop, 1)} sec")
        return self.M

    def parallel(self, num, new_maps):
        results = [0] * num
        threads = [0] * num
        maps = [[] for i in range(num)]
        # map_keys = [x for x in new_maps.keys()]
        for i in range(len(new_maps)):
            val = new_maps[i]
            thread = i % num
            maps[thread].append(val)
        for i in range(num):
            threads[i] = threading.Thread(target=self.every_thread, args=(results, maps[i], i,))
            threads[i].start()
        for i in range(num):
            threads[i].join()
        allkey = functools.reduce(set.union, map(set, map(dict.keys, results)))

        new_pairs_dic = {}
        for key in allkey:
            new_pairs_dic[key] = {}
            results_key = [dict_i[key] if key in dict_i else {} for dict_i in results]
            all_pairs_for_key = functools.reduce(set.union, map(set, map(dict.keys, results_key)))
            for pair in all_pairs_for_key:
                sum = 0
                for result in results:
                    if key in result:
                        if pair in result[key]:
                            sum += result[key][pair]
                my_pair = Pair(key, pair)
                my_pair.recommends = sum
                new_pairs_dic[key][pair] = my_pair
        return new_pairs_dic

    def every_thread(self, results, map, i):
        # print(f"thread = {i}")
        result = {}
        for pair in map:
            for graph1Neighbor in graph1[pair.a]:
                if graph1Neighbor not in result:
                    result[graph1Neighbor] = {}
                for graph2Neighbor in graph2[pair.b]:
                    if graph2Neighbor not in result[graph1Neighbor]:
                        result[graph1Neighbor][graph2Neighbor] = 0
                    result[graph1Neighbor][graph2Neighbor] += 1
        results[i] = result

    def main_loop0(self):
        Z = set()
        countLoops = 0
        hard_part = False
        A = self.loadSources()
        while len(A) != 0:
            countLoops += 1
            Z = self.add_mark_to_A_friends_parallel(A, Z, countLoops, first=(countLoops == 1))
            while (not self.queue.isEmpty()) and self.queue.top()[
                0] > 1000:  # and len(self.M) < 12232: ################################    len(self.M) < 12232
                pop = self.queue.pop()
                pair = pop[1]
                if pair.a in self.M or pair.b in self.unM:
                    continue
                Z = self.add_pair_to_map(pair, Z, hard_part)
            print(f"end of while {countLoops}. time: {time.time() - start}")
            A = self.update_A(Z)

    def main_loop(self, parallel, noisy_loop=False):
        if parallel:
            return self.main_loop_parallel(noisy_loop)
        threshold = 0 if noisy_loop else 1000
        Z = set()
        countLoops = 0
        hard_part = False
        A = self.loadSources()
        while len(A) != 0:
            countLoops += 1
            print(f"len(A)= {len(A)}")
            Z = self.add_mark_to_A_friends_parallel(A, Z, countLoops, first=(countLoops == 1))
            while (not self.queue.isEmpty()) and self.queue.top()[0] > threshold:
                pop = self.queue.pop()
                pair = pop[1]
                if pair.a in self.M or pair.b in self.unM:
                    continue
                Z = self.add_pair_to_map(pair, Z, hard_part)
            print(f"end of while {countLoops}. time: {time.time() - start}")
            A = self.update_A(Z)
            precision, recall = [], []
            self.evaluate([], recall, precision, True)
            # if precision[0] < 10 and recall[0] < 2:
            #     print("low precision - brook main loop")
            #     break
            # A = set()
        return self.M

    def main_loop_parallel(self, noisy_loop=False):
        last_maps = []
        threshold = 0 if noisy_loop else 1000
        Z = set()
        countLoops = 0
        A = self.loadSources()
        while len(A) != 0:
            countLoops += 1
            Z = self.add_mark_to_A_friends_parallel(A, Z, countLoops, first=(countLoops == 1))
            while ((not self.queue.isEmpty()) and self.queue.top()[0] > threshold) or len(last_maps) > 0:
                if len(last_maps) == self.params["parallel_batch"] or self.queue.isEmpty() or self.queue.top()[
                    0] <= threshold:
                    self.update_queue_parallel(last_maps, update_pairs_dic=True)
                    last_maps = []
                    continue
                # and len(self.M) < 12232: ################################    len(self.M) < 12232
                pop = self.queue.pop()
                pair = pop[1]
                if pair.a in self.M or pair.b in self.unM:
                    continue
                self.M[pair.a] = pair.b
                self.unM[pair.b] = pair.a
                self.update_plots(pair)
                if len(self.M) % 2000 == 0:
                    print("done: " + str(len(self.M)))
                if pair not in Z:
                    last_maps.append(Pair(pair.a, pair.b))
                    Z.add(pair)
                # Z = self.add_pair_to_map(pair, Z, hard_part)
            print(f"end of while {countLoops}. time: {time.time() - start}")
            A = self.update_A(Z)
            # A = set()
        return self.M

    def update_queue_parallel(self, last_maps, update_pairs_dic):
        new_marks = self.parallel(params["threads"], last_maps)
        for node1 in new_marks:
            if node1 not in self.pairs_dic:
                self.pairs_dic[node1] = {}
            for node2 in new_marks[node1]:
                if node2 not in self.pairs_dic[node1]:
                    temp_pair = Pair(node1, node2)
                    self.initialize_pair_values(temp_pair)
                    self.pairs_dic[node1][node2] = temp_pair
                pair = self.pairs_dic[node1][node2]
                add_recommends = new_marks[node1][node2].recommends
                pair.recommends += add_recommends
                simple_delta_deg = abs(len(self.graph1[pair.a]) - len(self.graph2[pair.b]))
                if self.queue.contains(pair):
                    self.queue.addToPriority(1000 * add_recommends, pair)
                else:
                    self.queue.addToPriority(1000 * add_recommends - simple_delta_deg, pair)

    def addMarkToFriends(self, recomending_pair):
        for graph1Neighbor in graph1[recomending_pair.a]:
            # if not self.M.__contains__(graph1Neighbor):
            for graph2Neighbor in graph2[recomending_pair.b]:
                # if not self.unM.__contains__(graph2Neighbor):
                # points = abs(data1[graph1Neighbor].friendsAvgDegree - data2[graph2Neighbor].friendsAvgDegree)
                pair_to_update = Pair(graph1Neighbor, graph2Neighbor)
                self.addMark(pair_to_update, recomending_pair)

    def addMark(self, pair, recommendPair, num=1):
        simple_delta_deg = abs(len(self.graph1[pair.a]) - len(self.graph2[pair.b]))

        if pair.a not in self.pairs_dic:
            self.pairs_dic[pair.a] = {}
        if pair.b not in self.pairs_dic[pair.a]:
            # self.initialize_pair_values(pair)
            self.pairs_dic[pair.a][pair.b] = pair
        else:
            pair = self.pairs_dic[pair.a][pair.b]
        # pair.recommends += num
        self.thin_update_pair_values(pair, recommendPair)

        # if False and hard_part:
        #     self.queue.update(self.ML_priority(pair), pair)
        if self.queue.contains(pair):
            self.queue.addToPriority(1000, pair)
        else:
            self.queue.addToPriority(1000 - simple_delta_deg, pair)  # - 2*friendsDeltaDeg-deltaDeg

    def reorderQueue(self):
        print("start reorder")
        new_queue = myQueue.MaxQueue()
        while not self.queue.isEmpty():
            pop = self.queue.pop()
            pair = pop[1]
            if self.M.__contains__(pair.a) or self.unM.__contains__(pair.b):
                continue
            self.initialize_pair_values(pair)
            self.update_pair_against_percent_b(pair)
            new_queue.update(self.hard_part_priority(pair), pair)
        print("finish reorder")
        self.queue = new_queue

    def ML_priority(self, pair):
        data = self.data_to_predict_regression(pair)
        for i in range(0, len(data)):
            data[i] = (data[i] - self.early_regression_mean[i]) / self.early_regression_std[i]
        # temp = self.early_regression.predict([data])[0]
        temp = self.early_regression.predict_proba([data])[0][1]
        # if temp == 0:
        #    print(temp)
        return temp

    def hard_part_priority(self, pair):
        # pair.against = self.countAgainst(pair) + 1
        # return self.new_hard_part_priority(pair)
        numerator = pair.adamicAdar * pair.recommends
        denominator = pair.against * pair.percentDeltaLogDegree ** 2 * pair.friendsPercentDeltaLogDegree
        return numerator / denominator

    def new_hard_part_priority(self, pair):
        stdA = self.graph1Data[pair.a].friendsDeviation
        stdB = self.graph2Data[pair.b].friendsDeviation
        values = [pair.log_degree_sum, pair.deltaLogDegree, pair.percentDeltaLogDegree, pair.friendsLogDegreeSum,
                  pair.friendsDeltaLogDegree,
                  pair.friendsPercentDeltaLogDegree, pair.recommends, pair.against, abs(stdA - stdB), stdA + stdB,
                  pair.adamicAdar]
        np_values = np.array(values)
        normilized = (np_values - self.early_regression_mean) / self.early_regression_std
        return_value = (normilized * self.early_regression).sum()
        # print(return_value)
        return return_value

    def initialize_pair_values(self, pair):
        pair.a_degree = len(self.graph1[pair.a])
        pair.b_degree = len(self.graph2[pair.b])
        degreeA = math.log(pair.a_degree + 1)
        degreeB = math.log(pair.b_degree + 1)
        pair.log_degree_sum = degreeB + degreeA
        pair.degree_sum = pair.a_degree + pair.b_degree
        return
        pair.deltaLogDegree = abs(degreeA - degreeB)
        pair.percentDeltaLogDegree = (2 * pair.deltaLogDegree + 1) / pair.log_degree_sum

        neighborsDegreeA = self.graph1Data[pair.a].friendsAvgLogDegree
        neighborsDegreeB = self.graph2Data[pair.b].friendsAvgLogDegree
        pair.friendsLogDegreeSum = neighborsDegreeA + neighborsDegreeB
        pair.friendsDeltaLogDegree = abs(neighborsDegreeA - neighborsDegreeB)
        pair.friendsPercentDeltaLogDegree = (2 * pair.friendsDeltaLogDegree + 1) / pair.friendsLogDegreeSum
        # pair.against = self.countAgainst(pair) + 1

    def thin_update_pair_values(self, pair, recommend_pair):
        pair.recommends += 1

    def update_pair_values(self, pair, recommend_pair):
        pair.recommends += 1
        pair.second_recommends += recommend_pair.recommends

        temp1 = math.log(len(self.graph1[recommend_pair.a]) + 1)
        temp2 = math.log(len(self.graph2[recommend_pair.b]) + 1)
        score = 1 / (temp1 + temp2)
        pair.adamicAdar += score
        self.update_pair_against_percent_b(pair)

    def update_pair_against_percent_b(self, pair):
        pair.recommend_percent = pair.recommends / (pair.a_degree + pair.b_degree)

        mapped_a_friends = pair.a_degree - self.graph1Data[pair.a].unMatched
        mapped_b_friends = pair.b_degree - self.graph2Data[pair.b].unMatched

        pair.against = mapped_a_friends + mapped_b_friends - 2 * pair.recommends + 1
        pair.against_percent = pair.against / (pair.a_degree + pair.b_degree)

    def update_pair_against_percent_y(self, pair):
        pair.recommend_percent = pair.recommends ** 2 / (len(self.graph1[pair.a]) * len(self.graph2[pair.b]))

        mapped_a_friends = len(self.graph1[pair.a]) - self.graph1Data[pair.a].unMatched
        mapped_b_friends = len(self.graph2[pair.b]) - self.graph2Data[pair.b].unMatched

        pair.against = mapped_a_friends * mapped_b_friends - pair.recommends ** 2 + 1
        pair.against_percent = pair.against / (len(self.graph1[pair.a]) * len(self.graph2[pair.b]))

    def data_to_predict_regression(self, pair):
        array = []
        array.append(pair.log_degree_sum)
        array.append(pair.deltaLogDegree)
        array.append(pair.percentDeltaLogDegree)

        array.append(pair.friendsLogDegreeSum)
        array.append(pair.friendsDeltaLogDegree)
        array.append(pair.friendsPercentDeltaLogDegree)

        stdA = self.graph1Data[pair.a].friendsDeviation
        stdB = self.graph2Data[pair.b].friendsDeviation
        array.append(abs(stdA - stdB))
        array.append(stdA + stdB)

        array.append(pair.adamicAdar)
        array.append(pair.recommends)

        self.update_pair_against_percent_b(pair)
        array.append(pair.against)
        array.append(len(self.M))
        return array

    def addToregression(self, pair, regression):  # , recommendPair, against
        regression["pairDegree"].append(pair.log_degree_sum)
        regression["deltaDegree"].append(pair.deltaLogDegree)
        regression["percentDeltaDegree"].append(pair.percentDeltaLogDegree)

        regression["neighborsDegree"].append(pair.friendsLogDegreeSum)
        regression["deltaNeighborsDegree"].append(pair.friendsDeltaLogDegree)
        regression["friendsPercentDeltaDegree"].append(pair.friendsPercentDeltaLogDegree)

        stdA = self.graph1Data[pair.a].friendsDeviation
        stdB = self.graph2Data[pair.b].friendsDeviation
        regression["deltaStd"].append(abs(stdA - stdB))
        regression["std"].append(stdA + stdB)

        regression["score"].append(pair.adamicAdar)
        regression["recommends"].append(pair.recommends)

        # against = self.countAgainst(pair)
        self.update_pair_against_percent_b(pair)
        regression["against"].append(pair.against)

        result = 100 if pair.a == pair.b else 0
        regression["result"].append(result)
        regression["place"].append(len(self.M))

    def countAgainst(self, pair):
        # return random.uniform(0, 1)
        against = 0
        for friend in self.graph1[pair.a]:
            if self.M.__contains__(friend) and pair.b not in self.graph2[self.M[friend]]:
                against += 1
        for friend in self.graph2[pair.b]:
            if self.unM.__contains__(friend) and pair.a not in graph1[self.unM[friend]]:
                against += 1
        return against

    def loadSources(self):
        A = set()
        for source in self.sources:
            self.M[source] = source
            self.unM[source] = source
            A.add(Pair(source, source))
        return A

    def add_mark_to_A_friends_parallel(self, A, Z, countLoops, first=False):
        # if len(A) < 200:
        #     return self.add_mark_to_A_friends(A, Z, countLoops)
        formated_A = []
        for pair in A:
            Z.add(pair)
            formated_A.append(Pair(pair.a, pair.b))
        self.update_queue_parallel(formated_A, update_pairs_dic=first)
        print(f"finish add_mark_to_A_friends {countLoops}. time: {time.time() - start}")
        return Z

    def add_mark_to_A_friends(self, A, Z, countLoops):
        for pair in A:
            self.addMarkToFriends(pair)
            Z.add(pair)
        print(f"finish add_mark_to_A_friends {countLoops}. time: {time.time() - start}")
        return Z

    def update_A(self, Z):
        A = set()
        for a in self.M:
            b = self.M[a]
            for graph1Neighbor in self.graph1[a]:
                if graph1Neighbor not in self.M:
                    for graph2Neighbor in self.graph2[b]:
                        if graph2Neighbor not in self.unM:
                            if Pair(graph1Neighbor, graph2Neighbor) not in Z:
                                A.add(Pair(graph1Neighbor, graph2Neighbor))
        return A

    def add_pair_to_map(self, pair, Z, hard_part):
        self.update_plots(pair)
        self.M[pair.a] = pair.b
        self.unM[pair.b] = pair.a
        if len(self.M) % 2000 == 0:
            print("done: " + str(len(self.M)))
        if pair not in Z:
            self.addMarkToFriends(pair)
            Z.add(pair)
        return Z

    def special_remove(self, pair):
        if len(self.plots.against_divide_recommends) <= 300:
            return False
        count = 0
        self.update_pair_against_percent_b(pair)
        val = pair.against_percent / pair.recommend_percent
        for i in range(0, 300):
            if val > self.plots.against_divide_recommends[-1 * i]:
                count += 1
        if count > 285:
            self.plots.against_divide_recommends.append(val)
            return True
        return False

    def update_plots(self, pair, pos=-1):
        val = 1 if pair.a == pair.b else 0
        if pos == -1:
            pos = len(self.M) - len(self.sources)
        self.plots.success_rate.append(val)
        # self.update_pair_against_percent_b(pair)
        # self.plots.against_divide_recommends.append(pair.against_percent / pair.recommend_percent)

        if val == 1:
            self.plots.correct_map_pos.append(pos)
            self.plots.correct_map_degree.append(pair.degree_sum / 2)
            self.plots.correct_map_log_degree.append(pair.log_degree_sum / 2)
            self.plots.correct_map_recommends.append(pair.recommends)
            return
            self.plots.correct_map_against.append(pair.against)
            self.plots.correct_recommends_percent_b.append(pair.recommend_percent)
            self.plots.correct_against_percent_b.append(pair.against_percent)
            self.plots.correct_against_divide_recommends.append(pair.against_percent / pair.recommend_percent)
            self.plots.correct_delta_degree.append(pair.percentDeltaLogDegree)
            self.plots.correct_friend_delta_degree.append(pair.friendsPercentDeltaLogDegree)
        else:
            self.plots.wrong_map_pos.append(pos)
            self.plots.wrong_map_degree.append(pair.degree_sum / 2)
            self.plots.wrong_map_log_degree.append(pair.log_degree_sum / 2)
            self.plots.wrong_map_recommends.append(pair.recommends)
            return
            self.plots.wrong_map_against.append(pair.against)
            self.plots.wrong_recommends_percent_b.append(pair.recommend_percent)
            self.plots.wrong_against_percent_b.append(pair.against_percent)
            self.plots.wrong_against_divide_recommends.append(pair.against_percent / pair.recommend_percent)
            self.plots.wrong_delta_degree.append(pair.percentDeltaLogDegree)
            self.plots.wrong_friend_delta_degree.append(pair.friendsPercentDeltaLogDegree)
        self.update_pair_against_percent_y(pair)
        if val == 1:
            self.plots.correct_recommends_percent_y.append(pair.recommend_percent)
            self.plots.correct_against_percent_y.append(pair.against_percent)
        else:
            self.plots.wrong_recommends_percent_y.append(pair.recommend_percent)
            self.plots.wrong_against_percent_y.append(pair.against_percent)
        self.update_pair_against_percent_b(pair)

    def save_plots(self, global_success_num, use_smooth=False):
        eps = False
        directory = self.params["plotDir"]
        smooth = self.params["smooth"] if use_smooth else 1
        # os.mkdir(directory)
        good_pos = self.plots.correct_map_pos[0:len(utils.smood(self.plots.correct_map_pos, smooth))]
        smooded_wrong_map_pos = utils.smood(self.plots.wrong_map_pos, smooth, good_pos[-1])
        last_pos = len(smooded_wrong_map_pos)
        bad_pos = self.plots.wrong_map_pos[0:last_pos]

        degs = [len(self.graph1[a]) for a in self.graph1]
        deg_counter = (np.array(degs).max() + 1) * [0]
        for deg in degs:
            deg_counter[deg] += 1
        deg_places = []
        for i in range(len(deg_counter)):
            deg_counter[i] /= len(self.graph1)
        plt.plot(deg_counter)
        x, y, title, file_name = "Degree", "Frequency ", "Degree distribution", directory + "Degree distribution"
        generic_plot(x, y, title, file_name, eps)

        plt.plot(utils.smood(self.plots.success_rate, 30))
        x, y, title, file_name = "Time", "Percents", "Precision (sliding window of 30)", directory + "success percent"
        generic_plot(x, y, title, file_name, eps)

        plt.plot(good_pos, utils.smood(self.plots.correct_map_log_degree, smooth), label='Correct pairs', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_log_degree, smooth), label='Wrong pairs', alpha=0.7)
        x, y, title, file_name = "Time", "Log degree", "Log degree of mapped pairs by order of insertion", directory + "Log degree"
        generic_plot(x, y, title, file_name, eps)

        plt.plot(good_pos, utils.smood(self.plots.correct_map_degree, smooth), label='Correct pairs', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_degree, smooth), label='Wrong pairs', alpha=0.7)
        x, y, title, file_name = "Time", "Degree", "Degree of mapped pairs by order of insertion", directory + "degree"
        generic_plot(x, y, title, file_name, eps)

        plt.plot(good_pos, utils.smood(self.plots.correct_map_recommends, smooth), label='correct pairs',
                 alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_recommends, smooth), label='wrong pairs',
                 alpha=0.7)
        x, y, title, file_name = "Time", "Marks", "Marks of mapped pairs by order of insertion", directory + "Marks"
        generic_plot(x, y, title, file_name, eps)

        return
        plt.plot(good_pos, utils.smood(self.plots.correct_map_against, smooth), label='Discouragements of correct',
                 alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_map_against, smooth), label='Discouragements of wrong',
                 alpha=0.7)
        plt.title("Discouragements of mapped pairs")
        plt.legend()
        plt.savefig(directory + "Discouragements")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_delta_degree, smooth), label='Delta degree of correct pairs',
                 alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_delta_degree, smooth), label='Delta degree of wrong pairs',
                 alpha=0.7)
        plt.title("Delta degree of mapped pairs in percents")
        plt.legend()
        plt.savefig(directory + "delta degree")
        plt.clf()

        plt.plot(bad_pos, utils.smood(self.plots.wrong_friend_delta_degree, smooth),
                 label='Friends_delta_degree of wrong pairs', alpha=0.7)
        plt.plot(good_pos, utils.smood(self.plots.correct_friend_delta_degree, smooth),
                 label='Friends_delta-degree_of correct pairs', alpha=0.7)
        plt.title("Delta average degree of mapped pairs' neighbors in percents")
        plt.legend()
        plt.savefig(directory + "delta neighbors degree")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_recommends_percent_b, smooth),
                 label='Recommendations percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_recommends_percent_b, smooth),
                 label='Recommendations percent of wrong', alpha=0.7)
        plt.title("Barak-Recommendations percent of mapped pairs")
        plt.legend()
        plt.savefig(directory + "Barak-recommends_percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_against_percent_b, smooth),
                 label='Discouragements percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_against_percent_b, smooth),
                 label='Discouragements percent of wrong', alpha=0.7)
        plt.title("Barak-Discouragements percent of mapped pairs")
        plt.legend()
        plt.grid()
        plt.savefig(directory + "Barak-discouragements_percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_against_divide_recommends, smooth),
                 label='correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_against_divide_recommends, smooth),
                 label='wrong', alpha=0.7)
        plt.title("Barak-discouragements/recommends percent of mapped pairs")
        plt.legend()
        plt.grid()
        plt.savefig(directory + "Barak-discouragements divide by recommends")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_against_percent_y, smooth),
                 label='Discouragements percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_against_percent_y, smooth),
                 label='Discouragements percent of wrong', alpha=0.7)
        plt.title("Discouragements percent of mapped pairs - Yoram")
        plt.legend()
        plt.savefig(directory + "Yoram-discouragements_percent")
        plt.clf()

        plt.plot(good_pos, utils.smood(self.plots.correct_recommends_percent_y, smooth),
                 label='Recommendations percent of correct', alpha=0.7)
        plt.plot(bad_pos, utils.smood(self.plots.wrong_recommends_percent_y, smooth),
                 label='Recommendations percent of wrongt', alpha=0.7)
        plt.title("Recommendations percent of mapped pairs - Yoram")
        plt.legend()
        plt.savefig(directory + "Yoram-recommends_percent")
        plt.clf()

    def different_in_M(self, last_M, last_unM):
        count = 0
        for v in self.M:
            if v not in last_M:
                count += 1
            elif self.M[v] != last_M[v]:
                count += 1
        for u in self.unM:
            if u not in last_unM:
                count += 1
            elif self.unM[u] != last_unM[u]:
                count += 1
        return round(count / len(self.M), 2)

    def analyzeRegression(self, regression_data):
        # print("***analyzeRegression***")
        std_array = []
        mean_array = []
        keys = [key for key in regression_data.keys()]
        for key in keys:
            if key == "result":
                continue
            array = np.array(regression_data[key])
            avg = array.sum() / array.size
            std = array.std()
            regression_data[key] = ((array - avg) / std).tolist()

            std_array.append(std)
            mean_array.append(avg)
            # array3 = array**3
            # avg3 = array3.sum() / array3.size
            # std3 = array3.std()
            # self.regression[key+"3"] = ((array3 - avg3) / std3).tolist()

        self.early_regression_std = np.array(std_array)
        self.early_regression_mean = np.array(mean_array)

        df = pd.DataFrame(regression_data,
                          columns=["pairDegree", "deltaDegree", "percentDeltaDegree", "neighborsDegree",
                                   "deltaNeighborsDegree",
                                   "friendsPercentDeltaDegree", "deltaStd", "std", "score", "recommends", "against",
                                   "place",
                                   # "pairDegree3", "deltaDegree3", "neighborsDegree3", "deltaNeighborsDegree3",
                                   # "recommends3", "score3", "deltaStd3", "std3", "against3",
                                   "result"])

        X = df[["pairDegree", "deltaDegree", "percentDeltaDegree", "neighborsDegree", "deltaNeighborsDegree",
                "friendsPercentDeltaDegree", "deltaStd", "std", "score", "recommends", "against", "place"]]
        # "pairDegree3", "deltaDegree3", "neighborsDegree3", "deltaNeighborsDegree3", "score3", "recommends3", "against3"]]
        Y = df["result"]
        regression = linear_model.LogisticRegression(penalty='l1',
                                                     solver='liblinear')  # LogisticRegression(penalty='l1', solver='liblinear')  #LinearRegression()
        regression.fit(X, Y)
        file = open("pickle_reggresion", 'wb')
        pickle.dump(regression, file)
        file.close()
        file = open("pickle_std", 'wb')
        pickle.dump(std_array, file)
        file.close()
        file = open("pickle_avg", 'wb')
        pickle.dump(mean_array, file)
        file.close()

        print('Intercept: \n', regression.intercept_)
        print('Coefficients: \n', regression.coef_)

    def evaluate(self, s_f1, s_recall, s_precision, quite=False):
        if not quite:
            print("**evaluate**")
        countCorrect = 0
        correct, wrong = Data(0), Data(0)
        correct_delta_degree = 0
        wrong_delta_degree = 0
        for a in self.M:
            b = self.M[a]
            if a == b:
                countCorrect += 1
        precision = countCorrect / len(self.M)
        recall = countCorrect / self.nodes_to_match
        f1 = 200 * recall * precision / (recall + precision)

        count_edges = 0
        for v in self.graph1:
            if v not in self.M:
                continue
            for u in self.graph1[v]:
                if u in self.M and self.M[v] in self.graph2[self.M[u]]:
                    count_edges += 1

        if not quite:
            print(f"should match: {self.nodes_to_match}")
            print(f"tried: {len(self.M)}")
            print(f"correct: {countCorrect}")
            print(f"precision: {round(100 * precision, 3)}")
            print(f"recall: {round(100 * recall, 3)}")
            print(f"f1 = {round(f1, 3)}")
            print(f"num of correct edges = {count_edges}")
            print("***************\n")
            # self.save_plots(countCorrect, True)
        s_f1.append(round(f1, 2))
        s_recall.append(round(recall * 100, 2))
        s_precision.append(round(precision * 100, 2))
        return countCorrect, count_edges


params = json.load(open('config.json'))["gradualProb"]
# random.seed(1000)
# print(params["plotDir"])
# os.mkdir(params["plotDir"])
graph_num = params["correntGraph"]
# params["sources"] = params["relevantSeeds"][graph_num] #################
params["path"] = params["relevantGraphs"][graph_num]
original_seed = params["relevantSeeds"][params["correntGraph"]]  # params["sources"]
save_all_diff = []
for seed in [1, 2, 4, 8, 16]:  # 1, 3, 5, 10,
    seed_f1, seed_recall, seed_precision, seed_count_edges, seed_diff_m = [], [], [], [], []
    params["sources"] = int(seed * original_seed)
    s_array = [0.4, 0.5, 0.6, 0.7, 0.8]
    for s in s_array:
        params["falseEdges"] = s
        s_f1, s_recall, s_precision, s_count_edges, s_diff_m = [], [], [], [], []
        # graph1, graph2, sources, params, nodes_to_match = utils.generateGraphs(params)
        graph1, graph2, sources, params, nodes_to_match = utils.generateFileGraphs(params)
        pprint.pprint(params)
        start = time.time()
        myExpandWhenStuck = MyExpandWhenStuck(graph1, graph2, sources, params, nodes_to_match)
        # myExpandWhenStuck.main_loop(parallel=False)
        myExpandWhenStuck.main_loop(parallel=False)
        countCorrect, count_edges = myExpandWhenStuck.evaluate(s_f1, s_recall, s_precision)
        s_count_edges.append(count_edges)
        s_diff_m.append(1)
        last_count_edges = 0
        last_run_recomends = myExpandWhenStuck.pairs_dic
        last_M, last_unM = myExpandWhenStuck.M, myExpandWhenStuck.unM

        last_break_condition = []
        count_loops = 1
        count_reduce = 1
        noisy_loop, reduce_stage, expand_stage, infinity = False, False, False, True  ############################
        noisy_loop_num = 0
        diff_M = 1
        while noisy_loop or expand_stage or reduce_stage or infinity:
            print(f"last ratio of edges: {round(count_edges / (1 + last_count_edges), 3)}")
            print(f"last difference in M: {diff_M}")
            last_count_edges = count_edges
            if noisy_loop:
                noisy_loop_num = count_loops
                print("noisy loop")
            print(f"loop = {count_loops}\n")

            third_myExpandWhenStuck = MyExpandWhenStuck(graph1, graph2, sources, params, nodes_to_match)
            if infinity:
                third_myExpandWhenStuck.infinity_loop(last_run_recomends)
            else:
                third_myExpandWhenStuck.iterative_loop(last_run_recomends, noisy_loop)
            countCorrect, count_edges = third_myExpandWhenStuck.evaluate(s_f1, s_recall, s_precision)
            s_count_edges.append(count_edges)
            count_loops += 1
            # diff_M = third_myExpandWhenStuck.different_in_M(last_M, last_unM)
            s_diff_m.append(diff_M)
            if not infinity:
                last_run_recomends = third_myExpandWhenStuck.pairs_dic
                last_M, last_unM = third_myExpandWhenStuck.M, third_myExpandWhenStuck.unM

            if noisy_loop:
                last_break_condition = [count_edges, count_edges / len(third_myExpandWhenStuck.M), s_precision[-1],
                                        s_recall[-1],
                                        s_f1[-1]]
                noisy_loop, reduce_stage, expand_stage, = False, True, False
            elif expand_stage:
                if count_loops <= 3:
                    continue
                if count_edges < last_count_edges * 1.02:
                    noisy_loop = True
            elif reduce_stage:
                reduce_stage = True if count_reduce <= 3 else False
                # elif count_edges > 0.98 * last_count_edges:
                #     reduce_stage = False
                curr_break_condition = [count_edges, count_edges / len(third_myExpandWhenStuck.M), s_precision[-1],
                                        s_recall[-1],
                                        s_f1[-1]]
                diff = [count_reduce]
                for i in range(5):
                    diff.append(round(curr_break_condition[i] - last_break_condition[i], 2))
                save_all_diff.append(diff)
                last_break_condition = curr_break_condition
                count_reduce += 1
            if infinity:
                infinity, expand_stage = False, True

        print(f"S = {s}, seed={int(seed * original_seed)}:")
        s_f1.append(noisy_loop_num)
        print(s_f1)
        seed_diff_m.append(s_diff_m)
        seed_count_edges.append(s_count_edges)
        seed_f1.append(s_f1)
        seed_recall.append(s_recall)
        seed_precision.append(s_precision)
        print(f"time: {str(time.time() - start)}\n\n")

    print(f"seed = {int(seed * original_seed)}:")
    for i in range(len(s_array)):
        print(f"\"{s_array[i]}\": [")
        print(str(seed_diff_m[i]) + ",")
        print(str(seed_count_edges[i]) + ",")
        print(str(seed_precision[i]) + ",")
        print(str(seed_recall[i]) + ",")
        last_comma = "" if i == len(s_array) - 1 else ","
        print(str(seed_f1[i]) + "]" + last_comma)
        print("\n\n")

print("***breaking condition:***")
for a in save_all_diff:
    print(str(a) + ",")
