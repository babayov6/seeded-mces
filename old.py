def create_dicts_of_connections(set_proj_nodes, set_no_proj_nodes, neighbors_dict):
    """
    a function that creates 3 dictionaries:
    1. dict_node_node (explained below)
    2. dict_node_enode (explained below)
    2. dict_enode_enode (explained below)
    :param: set_proj_nodes: Set of nodes that are in the embedding
    :param: set_no_proj_nodes: Set of nodes that are not in the embedding
    :param: Neighbors_dict: Dictionary where value==node and key==set_of_neighbors
    :return: 3 nodes that were mentioned before
    """
    # value == (node that isn't in the embedding), key == (set of its neighbours that are also not in the embedding)
    dict_node_node = {}
    # value == (node that isn't in the embedding), key == (set of neighbours thar are in the embedding)
    dict_node_enode = {}
    # key==(node that is in the projection and has neighbors in it), value==(set of neighbors that are in projection)
    dict_enode_enode = {}
    # nodes that are not in the projection
    list_no_proj = list(set_no_proj_nodes)
    list_proj = list(set_proj_nodes)
    for i in range(len(list_no_proj)):
        node = list_no_proj[i]
        # neighbors of the node that aren't in the projection
        set1 = neighbors_dict[node].intersection(set_no_proj_nodes)
        dict_node_node.update({node: set1})
        # neighbors of the node that are in the projection
        set2 = neighbors_dict[node].intersection(set_proj_nodes)
        if len(set2) > 0:
            dict_node_enode.update({node: set2})
    for i in range(len(list_proj)):
        node = list_proj[i]
        # neighbors of the node that are in the projection
        set1 = neighbors_dict[node].intersection(set_proj_nodes)
        if len(set1) > 0:
            dict_enode_enode.update({node: set1})
    return dict_node_node, dict_node_enode, dict_enode_enode

def create_dict_neighbors(G):
    """
    Create a dictionary where value==node and key==set_of_neighbors.
    :param: G: Our graph
    :return: Dictionary of neighbours as described above.
    """
    G_nodes = list(G.nodes())
    neighbors_dict = {}
    for i in range(len(G_nodes)):
        node = G_nodes[i]
        neighbors_dict.update({node: set(G[node])})
    return neighbors_dict


def oneSourceDiffusion(g, source, k, RATE):
    index = 0
    # curr = [None]*g.GetNodes()
    curr = {}
    prev = {}
    # prev = [None]*g.GetNodes()
    curr[source] = 1000
    for node in g.Nodes():
        if (node.GetId() != source):
            curr[node.GetId()] = 0
    while (index < k):
        prev = curr
        for node in g.Nodes():
            i = node.GetId()
            # print("*****"+str(node.GetId())+"****")
            curr[i] = prev[i] - prev[i] * RATE * node.GetDeg()
            for edge in node.GetOutEdges():
                # print(edge)
                # print(prev[edge])
                curr[i] = curr[i] + RATE * prev[edge]
        index = index + 1

    return curr


def Diffusion(g, sources, k, RATE):
    answer = {}
    for source in sources:
        answer[source] = oneSourceDiffusion(g, source, k, RATE)
    return answer

if embeding == "diffusion":
    proj = Diffusion(g, sources, DIFF_ITERATIONS, DIFF_RATE)
    g_proj = diffProjToOgerProj(proj,sources, regularNodes)
    proj = Diffusion(h, sources, DIFF_ITERATIONS, DIFF_RATE)
    h_proj = diffProjToOgerProj(proj,sources, regularNodes)

    #disTable = findDistance(g_proj, h_proj, regularNodes, sources, IGNORE)
    #disTable = findDistanceForOger(g_proj, h_proj, regularNodes, sources, IGNORE)

def findDistance(gDiff, hDiff, nodes, sources, IGNOR):
    disTable = {}
    for v1 in nodes:
        disTable[v1] = {}
        for v2 in nodes:
            disTable[v1][v2] = 0
            disArr = []
            for source in sources:
                disArr.append((gDiff[source][v1] - hDiff[source][v2]) ** 2)
            disArr.sort()
            for i in range(0, len(disArr) - IGNOR):
                disTable[v1][v2] += disArr[i]
    return disTable


def matchBydisTable2(disTable, nodes1, nodes2):
    match = {}
    count = 0
    maxDis = -1
    maxDisNode = 0
    NearestNodePair = 0
    nodePair = 0
    minDis = {}
    for v1 in nodes1:
        dis = -1
        for v2 in nodes2:
            if (dis == -1 or disTable[v1][v2] < dis):
                # print(disTable[v1][v2])
                dis = disTable[v1][v2]
                nodePair = v2
        match[v1] = nodePair
        if (v1 == nodePair):
            count += 1
    # print("success = " + str(count))
    return count


def matchBydisTable(disTable, nodes1, nodes2):
    match = {}
    count = 0
    while (len(nodes1) > 0):
        pair = getNearestNode(disTable, nodes1, nodes2)
        match[pair[0]] = pair[1]
        # print(pair[0],pair[1])
        if (pair[0] == pair[1]):
            count += 1
        nodes1.remove(pair[0])
        nodes2.remove(pair[1])
    # print("success = " + str(count))
    return count


def getNearestNode(disTable, nodes1, nodes2):
    maxDis = -1
    maxDisNode = 0
    NearestNodePair = 0
    nodePair = 0
    minDis = {}
    for v1 in nodes1:
        dis = -1
        for v2 in nodes2:
            if (dis == -1 or disTable[v1][v2] < dis):
                # print(disTable[v1][v2])
                dis = disTable[v1][v2]
                nodePair = v2
        if (maxDis == -1 or dis < maxDis):
            maxDis = dis
            maxDisNode = v1
            NearestNodePair = nodePair
    return (maxDisNode, NearestNodePair)


def getFarestNode(disTable, nodes1, nodes2):
    maxDis = 0
    maxDisNode = 0
    FarestNodePair = 0
    nodePair = 0
    minDis = {}
    for v1 in nodes1:
        dis = -1
        for v2 in nodes2:
            if dis == -1 or disTable[v1][v2] < dis:
                # print(disTable[v1][v2])
                dis = disTable[v1][v2]
                nodePair = v2
        if dis >= maxDis:
            maxDis = dis
            maxDisNode = v1
            FarestNodePair = nodePair
    return maxDisNode, FarestNodePair



def findDistanceForOger(g_proj, h_proj, nodes, sources, IGNOR):
    for i in g_proj:
        if (i not in sources):
            temp_g = []
            temp_h = []
            for value in g_proj[i]:
                temp_g.append(value)
            g_proj[i] = temp_g
            for value in h_proj[i]:
                temp_h.append(value)
            h_proj[i] = temp_h
    disTable = {}
    for v1 in g_proj:
        if v1 not in sources:
            disTable[v1] = {}
            for v2 in h_proj:
                if v2 not in sources:
                    disTable[v1][v2] = 0
                    disArr = []
                    for i in range(0,len(sources)):
                        disArr.append((g_proj[v1][i]- h_proj[v2][i])**2)
                    disArr.sort()
                    for i in range(0,len(disArr)-IGNOR):
                        disTable[v1][v2] += disArr[i]
    return disTable


def DiffAndPM(file = None):
    start = time.time()
    IGNORE = 0
    DIFF_RATE = 0.005
    DIFF_ITERATIONS = 6

    FalseEdgesPercent = 1.00
    SOURCE_PERCENT = 0.05

    g = snapGraphFromFile(file)
    FalseEdges = int(FalseEdgesPercent*g.GetNodes())
    h = duplicate(g)
    g = addRandomEdges(g, FalseEdges)
    h = addRandomEdges(h, FalseEdges)
    sources = chooseSources(g, SOURCE_PERCENT)
    print("preper:" + str(time.time() - start))
    start = time.time()
    gDiff = Diffusion(g, sources, DIFF_ITERATIONS, DIFF_RATE)
    hDiff = Diffusion(h, sources, DIFF_ITERATIONS, DIFF_RATE)
    print("2 Diffusions:" + str(time.time() - start))
    start = time.time()
    regularNodes = notSources(g, sources)
    disTable = findDistance(gDiff, hDiff, regularNodes, sources, IGNORE)
    print("findDistance:" + str(time.time() - start))
    start = time.time()
    count = matchBydisTable2(disTable, regularNodes.copy(), regularNodes.copy())
    print("matchBydisTable:" + str(time.time() - start))
    print("sould match = " + str(len(regularNodes)))
    print("success rate = " + str(count*1.0 / len(regularNodes)))
    print("__________________________")
    return count*1.0 / len(regularNodes)


def OgerAndPM(file = None):
    IGNORE = 0
    FalseEdgesPercent = 1.0
    SOURCE_PERCENT = 0.05
    start = time.time()
    g = snapGraphFromFile(file)
    FalseEdges = int(FalseEdgesPercent * g.GetNodes())
    h = duplicate(g)
    g = addRandomEdges(g, FalseEdges)
    h = addRandomEdges(h, FalseEdges)
    sources = chooseSources(g, SOURCE_PERCENT)
    regularNodes = notSources(g, sources)
    dict_projections1 = {}
    dict_projections2 = {}
    i = 0
    for source in sources:
        dict_projections1[source] = [0]*len(sources)
        dict_projections1[source][i] = 1
        dict_projections2[source] = [0] * len(sources)
        dict_projections2[source][i] = 1
        i = i+1

    net_g = snapToNetworx(g)
    net_h = snapToNetworx(h)
    print("preper:" + str(time.time() - start) )
    start = time.time()
    g_proj, g_n_e = doOGRE(net_g,sources,regularNodes,dict_projections1)
    h_proj, f_n_e = doOGRE(net_h, sources, regularNodes, dict_projections2)
    print("2 ogers:" + str(time.time() - start))
    start = time.time()
    for i in g_proj:
        if(i not in sources):
            temp_g = []
            temp_h = []
            for value in g_proj[i]:
                temp_g.append(value)
            g_proj[i] = temp_g
            for value in h_proj[i]:
                temp_h.append(value)
            h_proj[i] = temp_h

    disTable = findDistanceForOger(g_proj, h_proj, regularNodes, sources, IGNORE)
    print("findDistanceForOger:" + str(time.time() - start))
    start = time.time()
    count = matchBydisTable2(disTable, regularNodes, regularNodes.copy())
    print("matchBydisTable:" + str(time.time() - start))
    print("sould match = " + str(len(regularNodes)))
    print("success rate = " + str(count*1.0 / len(regularNodes)))
    print("__________________________")
    return count*1.0 / len(regularNodes)
