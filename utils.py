import snap
import math

import networkx
import random
import zlib
import hashlib
import os
import matplotlib.pyplot as plt
import numpy as np
import myQueue


class Pair:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.a_degree = 0
        self.b_degree = 0
        self.log_degree_sum = 0
        self.deltaLogDegree = 0
        self.percentDeltaLogDegree = 0
        self.friendsLogDegreeSum = 0
        self.friendsDeltaLogDegree = 0
        self.friendsPercentDeltaLogDegree = 0
        self.adamicAdar = 0
        self.against = 0
        self.recommends = 0
        self.second_recommends = 0
        self.recommend_percent = 0
        self.against_percent = 0

    def __key(self):
        return str(self.a) + ',' + str(self.b)

    def __eq__(self, other):
        return (self.a == other.a) and (self.b == other.b)

    def __hash__(self):
        return hash(self.__key())
        # h = hashlib.md5(str.encode(self.__key())).hexdigest()
        # return int(h, 16)


def networkxGraphFromFile(path):
    delimitor = " "  # if path.split(".")[-1] == "mtx" else ","
    graph = networkx.Graph()
    f = open(path, "r")
    line = f.readline()
    while (line != ''):
        edge = line.split("\n")[0].split(delimitor)
        graph.add_node(int(edge[0]))
        graph.add_node(int(edge[1]))
        graph.add_edge(int(edge[0]), int(edge[1]))
        line = f.readline()
    f.close()
    return graph


def snapGraphFromFile(path):
    delimitor = " "  # if path.split(".")[-1] == "mtx" else ","
    graph = snap.TUNGraph.New()
    f = open(path, "r")
    line = f.readline()
    while (line != ''):
        edge = line.split("\n")[0].split(delimitor)
        if (graph.IsNode(int(edge[0])) == False):
            graph.AddNode(int(edge[0]))
        if (graph.IsNode(int(edge[1])) == False):
            graph.AddNode(int(edge[1]))
        graph.AddEdge(int(edge[0]), int(edge[1]))
        line = f.readline()
    return graph


def generateFileGraphs(params):
    graph = networkxGraphFromFile(params["directory"] + params["path"])
    # p = 1/(params["falseEdges"] + 1)
    p = params["falseEdges"]
    graph1, graph2 = remove_networkx_edges(graph, p)

    nodes_to_match = 0
    count1 = 0
    for node in graph:
        if graph1.has_node(node) and graph2.has_node(node):
            nodes_to_match += 1
            if len(graph1[node]) == 1 or len(graph2[node]) == 1:
                count1 += 1
    print(f"all nodes: {len(graph.nodes)}")
    print(f"nodes to match {nodes_to_match}")
    print(f"nodes hard to match {count1}")
    params["nodes"] = len(graph1.nodes)
    params["avgDeg"] = 2 * len(graph1.edges()) / len(graph1.nodes)

    sources = chooseSourcesFileGraph(graph, graph1, graph2, params["sources"]) #choose_high_deg_sources  chooseSourcesFileGraph
    print("seed: " + str(len(sources)))
    return graph1, graph2, sources, params, nodes_to_match


def generateGraphs(params):
    # G_src = networkx.powerlaw_cluster_graph(params["nodes"], m=int(params["avgDeg"] * 0.5), p=0.1)
    print("")
    G_src = networkx.erdos_renyi_graph(params["nodes"], params["avgDeg"]/params["nodes"])
    count = 0
    for node in G_src:
        if(len(G_src[node]) > 9):
            count +=1
    print(f"AAAAAAAA {count}")
    print("nodes: " + str(params["nodes"]) + " avg degree: " + str(2 * len(G_src.edges) / params["nodes"]))
    # s = 1 / (1 + params["falseEdges"])
    s = params["falseEdges"]
    graph1, graph2 = remove_networkx_edges(G_src, s)
    print(f"s = {s} => new avg degree: {2 * len(graph1.edges) / len(graph1.nodes)}\n")

    sources = chooseSources2(graph1, graph2, params["sources"])

    nodes_to_match = 0
    count1 = 0
    for node in graph1:
        if graph1.has_node(node) and graph2.has_node(node):
            nodes_to_match += 1
            if len(graph1[node]) == 1 or len(graph2[node]) == 1:
                count1 += 1
    nodes = params["nodes"]
    print(f"all nodes: {nodes}")
    print(f"nodes to match {nodes_to_match}")
    print(f"nodes hard to match {count1}")
    # params["nodes"] = len(graph1.nodes)
    # params["avgDeg"] = 2 * len(graph1.edges()) / len(graph1.nodes)

    return graph1, graph2, sources, params, nodes_to_match


def networxToSnap(g):
    G = snap.TUNGraph.New()
    for node in g.nodes():
        G.AddNode(node)
    for edge in g.edges():
        G.AddEdge(edge[0], edge[1])
    return G


def snapToNetworx(g):
    G = networkx.Graph()
    for node in g.Nodes():
        G.add_node(node.GetId())
    for edge in g.Edges():
        G.add_edge(edge.GetSrcNId(), edge.GetDstNId())
    delete = []
    for node in G:
        if len(G[node]) == 0:
            delete.append(node)
    for node in delete:
        G.remove_node(node)
    return G


def duplicate(g):
    h = snap.TUNGraph.New()
    for node in g.Nodes():
        h.AddNode(node.GetId())

    for edge in g.Edges():
        h.AddEdge(edge.GetSrcNId(), edge.GetDstNId())

    return h


def remove_networkx_edges(graph, s):
    random.uniform(0, 1)
    graph1 = networkx.Graph()
    graph2 = networkx.Graph()
    for edge in graph.edges:
        rand = random.uniform(0, 1)
        if rand < s:
            graph1.add_node(int(edge[0]))
            graph1.add_node(int(edge[1]))
            graph1.add_edge(int(edge[0]), int(edge[1]))
        rand = random.uniform(0, 1)
        if rand < s:
            graph2.add_node(int(edge[0]))
            graph2.add_node(int(edge[1]))
            graph2.add_edge(int(edge[0]), int(edge[1]))
    return graph1, graph2


def remove_random_edges(graph, s):
    for edge in graph.Edges():
        rand = random.uniform(0, 1)
        if rand > s:
            graph.DelEdge(edge.GetSrcNId(), edge.GetDstNId())
    return graph


def addRandomEdges(graph, num):
    while (num > 0):
        a = graph.GetRndNId()
        b = graph.GetRndNId()
        if ((a != b) and graph.IsEdge(a, b) == False):
            graph.AddEdge(a, b)
            num = num - 1
    return graph


def choose_high_deg_sources(graph, graph1, graph2, percent):
    num = math.ceil(percent * len(graph.nodes)) if percent < 1 else percent
    queue = myQueue.MaxQueue()
    for node in graph1:
        if node not in graph2:
            continue
        queue.push(len(graph1[node]) + len(graph2[node]), node)
    group = []
    while len(group) < num:
        group.append(queue.pop()[1])
    return group


def chooseSourcesFileGraph(graph, graph1, graph2, percent):
    num = math.ceil(percent * len(graph.nodes)) if percent < 1 else percent
    group = []
    while len(group) < num:
        rand = int(random.uniform(0, len(graph.nodes)))
        source = 0
        i = 0
        for source in graph.nodes:
            if i == rand:
                break
            i += 1
        if (not group.__contains__(source)) and (graph1.nodes.__contains__(source)) and (
        graph2.nodes.__contains__(source)):
            group.append(source)
    return group


def chooseSources(graph, percent):
    num = math.ceil(percent * graph.GetNodes()) if percent < 1 else percent
    group = []
    while len(group) < num:
        r = graph.GetRndNId()
        if group.count(r) == 0:
            group.append(r)
    return group


def chooseSources2(graph1, graph2, percent):
    num = math.ceil(percent * len(graph1.nodes)) if percent < 1 else percent
    percent = num / len(graph1.nodes)
    group = []
    for node in graph1.nodes:
        r = random.uniform(0, 1)
        if r < percent and node in graph2.nodes:
            group.append(node)
    return group


def notSources(g, sources):
    output = []
    for node in g.Nodes():
        v = node.GetId()
        if (sources.count(v) == 0):
            output.append(v)
    return output


def removeSources(proj, sources):
    for source in sources:
        proj.pop(source, None)
    return proj


def smood(array, k, compare = -1):
    if len(array) < k:
        return array
    sumK = 0
    newArray = []
    for i in range(0, k):
        sumK += array[i]
    for i in range(k, len(array)):
        newArray.append(sumK / k)
        sumK -= array[i - k]
        sumK += array[i]
    if newArray[-1] < 0.9*compare:
        return array
    return newArray


def save_plots(plots, directory, success_num, try_num, smooth):
    # directory = params["plotDir"] + dir_name + "/"
    os.mkdir(directory)
    good_pos = plots.correct_map_pos[0:len(smood(plots.correct_map_pos, smooth))]
    bad_pos = plots.wrong_map_pos[0:len(smood(plots.wrong_map_pos, smooth))]

    plt.plot(smood(plots.success_rate, 100))
    plt.title(f"Sliding window of success. total of {success_num} out of {try_num}")
    plt.savefig(directory + "success percent")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_map_degree, smooth), label='Correct')
    plt.plot(bad_pos, smood(plots.wrong_map_degree, smooth), label='Wrong')
    plt.title("Log degree of mapped pairs")
    plt.legend(loc="best")
    plt.savefig(directory + "log degree")
    plt.clf()

    plots.correct_second_recommends = np.array([math.log(x + 1) for x in plots.correct_second_recommends])
    plots.wrong_second_recommends = np.array([math.log(x + 1) for x in plots.wrong_second_recommends])
    plt.plot(good_pos, smood(plots.correct_second_recommends, smooth), label='Second recommendations of correct',
             alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_second_recommends, smooth), label='second recommendations of wrong', alpha=0.7)
    plt.title("Log second recommendations of mapped pairs*")
    plt.legend()
    plt.savefig(directory + "Second recommendations")
    plt.clf()

    plots.correct_map_recommends = np.array([math.log(x + 1) for x in plots.correct_map_recommends])
    plots.wrong_map_recommends = np.array([math.log(x + 1) for x in plots.wrong_map_recommends])
    plt.plot(good_pos, smood(plots.correct_map_recommends, smooth), label='Recommendations of correct', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_map_recommends, smooth), label='Recommendations of wrong', alpha=0.7)
    plt.title("Log recommendations of mapped pairs")
    plt.legend()
    plt.savefig(directory + "Recommendations")
    plt.clf()

    plots.correct_map_against = np.array([math.log(x + 2) for x in plots.correct_map_against])
    plots.wrong_map_against = np.array([math.log(x + 2) for x in plots.wrong_map_against])
    plt.plot(good_pos, smood(plots.correct_map_against, smooth), label='Discouragements of correct',
             alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_map_against, smooth), label='Discouragements of wrong',
             alpha=0.7)
    plt.title("Discouragements of mapped pairs")
    plt.legend()
    plt.savefig(directory + "Discouragements")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_delta_degree, smooth), label='Delta degree of correct pairs', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_delta_degree, smooth), label='Delta degree of wrong pairs', alpha=0.7)
    plt.title("Delta degree of mapped pairs in percents")
    plt.legend()
    plt.savefig(directory + "delta degree")
    plt.clf()

    plt.plot(bad_pos, smood(plots.wrong_friend_delta_degree, smooth),
             label='Friends_delta_degree of wrong pairs', alpha=0.7)
    plt.plot(good_pos, smood(plots.correct_friend_delta_degree, smooth), label='Friends_delta-degree_of correct pairs',
             alpha=0.7)
    plt.title("Delta average degree of mapped pairs' neighbors in percents")
    plt.legend()
    plt.savefig(directory + "delta neighbors degree")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_recommends_percent_b, smooth),
             label='Recommendations percent of correct', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_recommends_percent_b, smooth),
             label='Recommendations percent of wrong', alpha=0.7)
    plt.title("Barak-Recommendations percent of mapped pairs")
    plt.legend()
    plt.savefig(directory + "Barak-recommends_percent")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_against_percent_b, smooth),
             label='Discouragements percent of correct', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_against_percent_b, smooth),
             label='Discouragements percent of wrong', alpha=0.7)
    plt.title("Barak-Discouragements percent of mapped pairs")
    plt.legend()
    plt.grid()
    plt.savefig(directory + "Barak-discouragements_percent")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_against_divide_recommends, smooth),
             label='correct', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_against_divide_recommends, smooth),
             label='wrong', alpha=0.7)
    plt.title("Barak-discouragements/recommends percent of mapped pairs")
    plt.legend()
    plt.grid()
    plt.savefig(directory + "Barak-discouragements divide by recommends")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_against_percent_y, smooth),
             label='Discouragements percent of correct', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_against_percent_y, smooth),
             label='Discouragements percent of wrong', alpha=0.7)
    plt.title("Discouragements percent of mapped pairs - Yoram")
    plt.legend()
    plt.savefig(directory + "Yoram-discouragements_percent")
    plt.clf()

    plt.plot(good_pos, smood(plots.correct_recommends_percent_y, smooth),
             label='Recommendations percent of correct', alpha=0.7)
    plt.plot(bad_pos, smood(plots.wrong_recommends_percent_y, smooth),
             label='Recommendations percent of wrongt', alpha=0.7)
    plt.title("Recommendations percent of mapped pairs - Yoram")
    plt.legend()
    plt.savefig(directory + "Yoram-recommends_percent")
    plt.clf()
